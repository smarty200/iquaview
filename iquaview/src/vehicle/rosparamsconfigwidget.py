"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
 Class to read and store the xml structure associated to the ros_params tag in the AUV config file
"""
import logging
from lxml import etree

from qgis.gui import QgsCollapsibleGroupBox

from PyQt5.QtWidgets import QMessageBox

from iquaview_lib.xmlconfighandler.rosparamsreader import RosParamsReader, Section, Param
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser

from iquaview.src.vehicle.sectionwidget import SectionWidget
from iquaview.src.vehicle.paramwidget import ParamWidget
from iquaview.src.ui.ui_collapsible_groupbox import Ui_mCollapsibleGroupBox

logger = logging.getLogger(__name__)


class RosParamsConfigWidget(QgsCollapsibleGroupBox, Ui_mCollapsibleGroupBox):
    def __init__(self, config, vehicle_info, parent=None):
        """
        Constructor
        :param config: configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info: VehicleInfo
        """
        super(RosParamsConfigWidget, self).__init__(parent)
        self.setupUi(self)
        self.setTitle("ROS Params")
        self.add_pushButton.setText("New Section")
        self.config = config
        self.vehicle_info = vehicle_info
        self.filename = None
        self.section_list = list()
        self.section_key_count = 0

        self.add_pushButton.clicked.connect(lambda: self.new_ros_param_section())

        #load ros params from xml file
        self.set_config_filename(self.config.csettings['last_auv_config_xml'])

    def read_xml(self):
        """
        read the last auv configuration xml loaded
        """
        try:
            self.section_list.clear()
            ros_param_reader = RosParamsReader(self.filename,
                                               self.vehicle_info.get_vehicle_ip(),
                                               9091,
                                               self.vehicle_info.get_vehicle_namespace())
            section_list = ros_param_reader.read_configuration(get_ros_param=False)
            for section in section_list:
                self.add_section(section)
        except OSError as e:
            logger.error("OSError: {}".format(e))

    def set_config_filename(self, filename):
        """
        Set config
        :param filename: config filename
        :type filename: str
        """
        self.filename = self.config.csettings['configs_path'] + '/' + filename
        self.load_ros_params()

    def load_ros_params(self):
        """
        Remove previous widgets, read xml and load ros params
        """
        #remove previous widgets
        self.delete_all(self.vboxlayout.layout())

        self.read_xml()

        sections = self.get_sections()
        for section in sections:
            section_widget = self.add_section_widget(section.get_description(),
                                                     section.get_id(),
                                                     True)

            for param in section.get_params():
                param_widget = self.add_param_widget(section_widget,
                                                     param.get_name(),
                                                     param.get_description(),
                                                     param.get_action_id(),
                                                     param.get_id(),
                                                     True)

                param_widget.type_comboBox.setCurrentText(param.get_type())

                if not param.is_array() and param.has_range():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.range_radioButton.setChecked(True)

                    param_widget.range_lineEdit.setText(str(param.get_range())[1:-1])

                elif not param.is_array() and param.has_limits():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.limits_radioButton.setChecked(True)

                    param_widget.min_doubleSpinBox.setValue(param.get_min_limit())
                    param_widget.max_doubleSpinBox.setValue(param.get_max_limit())

                elif param.is_array():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.array_radioButton.setChecked(True)

                    param_widget.size_spinBox.setValue(int(param.get_array_size()))

                else:
                    param_widget.optional_checkBox.setChecked(False)

                param_widget.remove_pushButton.clicked.connect(lambda state, x=param_widget: self.remove_ros_param(x))
                param_widget.name_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.type_comboBox.currentIndexChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.description_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.action_id_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.optional_checkBox.stateChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.range_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.limits_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.array_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.range_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.min_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.max_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.size_spinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))

    def new_section(self, description):
        """
        Create new Section and add it to list
        :param description: section description
        :type description: str
        :return: Section
        """
        section = Section()
        section.set_description(description)
        section.set_id(self.section_key_count)

        self.section_list.append(section)
        self.section_key_count += 1

        return section

    def add_section(self, section):
        """
        add Section to section list
        :param section: Section to add
        :type section: Section
        """
        section.set_id(self.section_key_count)

        self.section_list.append(section)
        self.section_key_count += 1

    def get_sections(self):
        """
        :return: sections list
        :rtype: list
        """
        return self.section_list

    def get_section_by_id(self, id):
        """
        Get section by id
        :param id: section id
        :return: if found return section, otherwise return None
        """
        for section in self.section_list:
            if section.get_id() == id:

                return section

        return None

    def remove_section_by_id(self, identifier):
        """
        Remove section by id
        :param identifier: id of the section to remove
        :return: return True if removed, otherwise False
        """
        for section in self.section_list:
            if section.get_id() == identifier:
                self.section_list.remove(section)
                return True

        return False

    def new_ros_param_section(self, description="Section"):
        """
        Create new section
        :param description: section description
        :type description: str
        """

        section = self.new_section(description)
        self.add_section_widget(description, section.get_id())

    def add_section_widget(self, description="Section", id=None, collapsed=False):
        """
        Add section to layout
        :param description: section description
        :type description: str
        :param id: id to identify section
        :type id: int
        :param collapsed:  collapsed state
        :type collapded: bool
        :return: return section widget
        :rtype SectionWidget
        """
        section_widget = SectionWidget(description, id, collapsed)
        self.vboxlayout.layout().addWidget(section_widget)

        section_widget.remove_section_pushButton.clicked.connect(lambda state, x=section_widget: self.remove_ros_param_section(x))
        section_widget.add_param_pushButton.clicked.connect(lambda state, x=section_widget:  self.new_ros_param(section_widget=x))
        section_widget.description_lineEdit.textChanged.connect(lambda state, x=section_widget: self.update_section(x))

        section_widget.description_lineEdit.setFocus()

        return section_widget

    def new_ros_param(self, section_widget, name="", description="Param", action_id=""):
        """
        Create new param
        :param section_widget: widget where the parameter will be added
        :type section_widget: SectionWidget
        :param name: param name
        :type name: str
        :param description: param description
        :type description: str
        """
        section = self.get_section_by_id(int(section_widget.objectName()))
        param = Param(description=name)
        identifier = section.add_param(param)
        param_widget = self.add_param_widget(section_widget, name, description, action_id, identifier)
        param_widget.remove_pushButton.clicked.connect(lambda state, x=param_widget: self.remove_ros_param(x))
        param_widget.name_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.type_comboBox.currentIndexChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.description_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.action_id_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.optional_checkBox.stateChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.range_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.limits_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.array_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.range_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.min_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.max_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.size_spinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))

    def add_param_widget(self, section, name="", description="Param",  action_id=None, id=None, collapsed=False):
        """
        Add param to section layout
        :param section: section widget
        :type section: SectionWidget
        :param name: param name
        :type name: str
        :param description: param description
        :type description: str
        :param id: id to identify param
        :type id: int
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return param widget
        :rtype: ParamWidget
        """
        param_widget = ParamWidget(name, description, action_id, id, collapsed, section)

        section.params_vboxlayout.layout().addWidget(param_widget)

        param_widget.limits_radioButton.toggle()
        param_widget.range_radioButton.toggle()
        param_widget.optional_checkBox.toggle()

        param_widget.optional_checkBox.setChecked(False)

        param_widget.name_lineEdit.setFocus()

        return param_widget

    def remove_ros_param_section(self, section_widget):
        """
        remove section
        :param section_widget: section widget to remove
        :type section_widget: SectionWidget
        """
        confirmation_msg = "Are you sure you want to remove {} section?".format(section_widget.title())

        reply = QMessageBox.question(self, 'Remove confirmation',
                                     confirmation_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            removed = self.remove_section_by_id(int(section_widget.objectName()))
            self.delete_all(section_widget.layout())
            section_widget.deleteLater()

    def remove_ros_param(self, param_widget):
        """
        remove param
        :param param_widget: param widget to remove
        :type param_widget: ParamWidget
        """
        confirmation_msg = "Are you sure you want to remove {} parameter?".format(param_widget.title())

        reply = QMessageBox.question(self, 'Remove confirmation',
                                     confirmation_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            section_widget = param_widget.get_section_widget()
            section = self.get_section_by_id(int(section_widget.objectName()))
            params = section.get_params()
            for param in params:
                if param.get_id() == int(param_widget.objectName()):
                    section.get_params().remove(param)

            self.delete_all(param_widget.layout())
            param_widget.deleteLater()

    def update_section(self, widget):
        """
        Update section from widget
        :type widget: SectionWidget with the information
        :param widget: SectionWidget
        """
        section = self.get_section_by_id(int(widget.objectName()))
        section.set_description(widget.description_lineEdit.text())

    def update_param(self, param_widget):
        """
        Update param from widget
        :param param_widget: widget with the information
        :type param_widget: ParamWidget
        """
        section_widget = param_widget.get_section_widget()
        section = self.get_section_by_id(int(section_widget.objectName()))
        params = section.get_params()
        for param in params:
            if param.get_id() == int(param_widget.objectName()):

                if param_widget.optional_checkBox.isChecked() and param_widget.array_radioButton.isChecked():
                    param.set_field_array(param_widget.name_lineEdit.text(),
                                          param_widget.type_comboBox.currentText(),
                                          str(param_widget.size_spinBox.value()))
                    param.set_description(param_widget.description_lineEdit.text())
                    param.set_action_id(param_widget.action_id_lineEdit.text())
                    param.field = None
                else:
                    param.field_array = None
                    param.set_field(param_widget.name_lineEdit.text(),
                                    param_widget.type_comboBox.currentText())
                    param.set_description(param_widget.description_lineEdit.text())
                    param.set_action_id(param_widget.action_id_lineEdit.text())

                    if param_widget.optional_checkBox.isChecked():
                        if param_widget.range_radioButton.isChecked():
                            if param_widget.range_lineEdit.text() != "":
                                param.set_field_range(param_widget.range_lineEdit.text())
                        else:
                            param.set_field_limits(param_widget.min_doubleSpinBox.value(),
                                                   param_widget.max_doubleSpinBox.value())

    def save(self):
        """
        save the ros params inside the auv configuration xml
        """

        config_parser = XMLConfigParser(self.filename)
        # get ros_params
        ros_params = config_parser.first_match(config_parser.root, "ros_params")
        # all sections in ros_params
        ros_params.clear()

        for section in self.section_list:
            xml_section = etree.SubElement(ros_params, "section")

            xml_section_description = etree.SubElement(xml_section, "description")

            xml_section_description.text = section.get_description()

            for param in section.get_params():
                xml_param = etree.SubElement(xml_section, "param")

                xml_param_description = etree.SubElement(xml_param, "description")
                xml_param_description.text = param.get_description()

                xml_param_action_id = etree.SubElement(xml_param, "action_id")
                xml_param_action_id.text = param.get_action_id()

                if param.is_array():
                    xml_field_array = etree.SubElement(xml_param, "field_array")

                    xml_field_array_name = etree.SubElement(xml_field_array, "field_array_name")
                    xml_field_array_type = etree.SubElement(xml_field_array, "field_array_type")
                    xml_field_array_size = etree.SubElement(xml_field_array, "field_array_size")

                    xml_field_array_name.text = param.get_name()
                    xml_field_array_type.text = param.get_type()
                    xml_field_array_size.text = param.get_array_size()

                else:
                    xml_field = etree.SubElement(xml_param, "field")

                    xml_field_name = etree.SubElement(xml_field, "field_name")
                    xml_field_type = etree.SubElement(xml_field, "field_type")

                    xml_field_name.text = param.get_name()
                    xml_field_type.text = param.get_type()

                    if param.has_range():
                        xml_field_range = etree.SubElement(xml_field, "field_range")

                        xml_field_range.text = str(param.get_range())[1:-1]

                    elif param.has_limits():
                        xml_field_limits = etree.SubElement(xml_field, "field_limits")
                        xml_field_min = etree.SubElement(xml_field_limits, "min")
                        xml_field_max = etree.SubElement(xml_field_limits, "max")

                        xml_field_min.text = str(param.get_min_limit())
                        xml_field_max.text = str(param.get_max_limit())

        config_parser.write()

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())