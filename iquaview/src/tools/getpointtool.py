"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
Map tool for get a point from the map canvas.
"""

from qgis.core import QgsPointXY
from qgis.gui import QgsMapTool
from PyQt5.QtCore import pyqtSignal


class GetPointTool(QgsMapTool):
    point_signal = pyqtSignal(QgsPointXY)

    def __init__(self, canvas):
        """
        Init of the object AddLandmarkTool
        :param canvas: Canvas where the tool actuates
        :type canvas: QgsMapCanvas
        """
        super().__init__(canvas)
        self.point = None
        self.canvas = canvas

    def canvasReleaseEvent(self, e):
        """
        Overrides method canvasReleaseEvent from QgsMapTool.
        Emits a signal with the point of the event
        :param e: event
        :type e: QgsMapMouseEvent
        """
        self.point = self.toMapCoordinates(e.pos())
        self.point_signal.emit(self.point)

    def deactivate(self):
        """
        Overrides method deactivate from QgsMapTool.
        Calls method deactivate from QgsMapTool.
        """
        super(GetPointTool, self).deactivate()
