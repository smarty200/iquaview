"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget for converting lat lon coordinates between
 decimal degrees/degrees decimal minutes/degrees minutes seconds
"""
import logging
from geopy import point

from iquaview.src.ui.ui_coordinateconverterwidget import Ui_CoordinateConverterDialog

from PyQt5.QtWidgets import QDialog, QLineEdit, QSizePolicy
from PyQt5.QtCore import pyqtSignal, Qt

logger = logging.getLogger(__name__)


class CoordinateConverterDialog(QDialog, Ui_CoordinateConverterDialog):

    def __init__(self, parent=None):
        super(CoordinateConverterDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Coordinate Converter")

        self.coordinate_converter_lineedit = CoordinateConverterLineEdit()

        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.coordinate_converter_lineedit.sizePolicy().hasHeightForWidth())
        self.coordinate_converter_lineedit.setSizePolicy(sizePolicy)
        self.gridLayout.addWidget(self.coordinate_converter_lineedit, 0, 1)

        self.coordinate_converter_lineedit.converted_signal.connect(self.update_values)

    def update_values(self):
        sender = self.sender()
        self.degree_latlon_label.setText(sender.get_decimal_degrees())
        self.degreeminute_latlon_label.setText(sender.get_degrees_minutes())
        self.degreeminutesecond_latlon_label.setText(sender.get_degrees_minutes_seconds())


# coordinate converter LineEdit
class CoordinateConverterLineEdit(QLineEdit):
    converted_signal = pyqtSignal()

    def __init__(self):
        super(CoordinateConverterLineEdit, self).__init__()
        self.setAlignment(Qt.AlignCenter)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.setPlaceholderText("latitude, longitude")

        self.latitude = None
        self.longitude = None
        self.decimal_degrees = "000.0000º, 000.0000º"
        self.degrees_minutes = "000º00.00', 000º00.00'"
        self.degrees_minutes_seconds = "000º00'00.000'', 000º00'00.000''"

        self.textChanged.connect(self.convert)

    def convert(self):
        """Convert current coordinate format to others coordinate formats.
        example:
            DDD.DDDDº       -> DDDºMM.MM' and DDDºMM'SS.SSS''
            DDDºMM.MM'      -> DDD.DDDDº and DDDºMM'SS.SSS''
            DDDºMM'SS.SSS'' -> DDD.DDDDº and DDDºMM.MM'
        """
        color = ''  # white

        try:
            # get text
            lat_lon = self.text()
            lat_lon = lat_lon.replace("º", " ")

            point_lat_lon = point.Point(lat_lon)

            x = point_lat_lon.longitude
            y = point_lat_lon.latitude
            ew_str = "E"
            if x < 0:
                ew_str = "W"
                x = -x
            ns_str = "N"
            if y < 0:
                ns_str = "S"
                y = -y

            x_deg = int(x)
            y_deg = int(y)
            x_min = (x - x_deg) * 60
            y_min = (y - y_deg) * 60

            x_sec = (x - x_deg - int(x_min) / 60) * 3600
            y_sec = (y - y_deg - int(y_min) / 60) * 3600

            x_dd = "{:013.9f}º".format(x)
            y_dd = "{:012.9f}º".format(y)

            x_dm = "{:03d}º {:010.7f}'".format(x_deg, x_min)
            y_dm = "{:02d}º {:010.7f}'".format(y_deg, y_min)

            x_dms = "{:03d}º {:02d}' {:08.5f}\"".format(x_deg, int(x_min), x_sec)
            y_dms = "{:02d}º {:02d}' {:08.5f}\"".format(y_deg, int(y_min), y_sec)

            self.latitude = point_lat_lon.latitude
            self.longitude =  point_lat_lon.longitude
            self.decimal_degrees = "{} {}, {} {}".format(y_dd, ns_str, x_dd, ew_str)
            self.degrees_minutes = "{} {}, {} {}".format(y_dm, ns_str, x_dm, ew_str)
            self.degrees_minutes_seconds = "{} {}, {} {}".format(y_dms, ns_str, x_dms, ew_str)

        except:
            self.latitude = None
            self.longitude =  None
            self.decimal_degrees = "000.0000º, 000.0000º"
            self.degrees_minutes = "000º00.00', 000º00.00'"
            self.degrees_minutes_seconds = "000º00'00.000'', 000º00'00.000''"
            if self.text() != '':
                color = '#f6989d'  # red

        self.setStyleSheet('QLineEdit { background-color: %s }' % color)
        self.converted_signal.emit()

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_decimal_degrees(self):
        return self.decimal_degrees

    def get_degrees_minutes(self):
        return self.degrees_minutes

    def get_degrees_minutes_seconds(self):
        return self.degrees_minutes_seconds
