"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to show tooltips when the mouse pointer is over a layer without moving 
"""

import logging

from PyQt5.QtWidgets import QToolTip
from PyQt5.QtCore import QTimer

from qgis.gui import QgsMapTool
from qgis.core import QgsRectangle, QgsProject, QgsCoordinateTransform, QgsFeatureRequest, QgsFeature, QgsVectorLayer

logger = logging.getLogger(__name__)


class MapTip:
    def __init__(self, canvas):
        """
        Init of the object MapTip. Creates a timer to show the tool tip message
        :param canvas: canvas where the map tip will show
        :type canvas: QgsMapCanvas
        """
        self.canvas = canvas

        # create timer for map tips
        self.map_tips_timer = QTimer(self.canvas)
        self.map_tips_timer.timeout.connect(self.show_map_tip)
        self.map_tips_timer.setInterval(850)
        self.map_tips_timer.setSingleShot(True)

        # every time the mouse moves over the canvas, reset map_tips_timer
        self.canvas.xyCoordinates.connect(self.reset_map_tip)

    def reset_map_tip(self):
        """ Resets map tip text and timer """
        if QToolTip.isVisible():
            QToolTip.hideText()
        if self.canvas.underMouse():
            self.map_tips_timer.start()

    def show_map_tip(self):
        """ Shows name of the layer the mouse is hovering over, if any. """
        if self.canvas.underMouse():
            pos = self.canvas.mouseLastXY()
            layer_name = self.get_name_of_layer_at(pos)
            if layer_name:
                QToolTip.showText(self.canvas.mapToGlobal(pos), layer_name)

    def get_name_of_layer_at(self, pos):
        """
        Given a canvas position, returns the name of the vector layer that is in that position
        :param pos: Position in canvas XY coordinates
        :type pos:
        :return: Name of the vector layer found in that position, None if no layer is there
        :rtype: str
        """
        layer_name = ""
        layers = self.canvas.layers()
        canvas_rect = QgsRectangle(0, 0, self.canvas.width(), self.canvas.height())
        canvas_crs = self.canvas.mapSettings().destinationCrs()
        coord_transform = self.canvas.getCoordinateTransform()

        for layer in layers:
            try:
                if type(layer) == QgsVectorLayer:
                    # check if layer is in the extent of the canvas
                    layer_extent = layer.extent()
                    layer_crs = layer.sourceCrs()

                    # first transform layer extent to match canvas crs
                    if layer_crs.authid() != canvas_crs.authid():
                        context = QgsProject.instance().transformContext()
                        crs_transform = QgsCoordinateTransform(layer_crs, canvas_crs, context)
                        top_left = crs_transform.transform(layer_extent.xMinimum(), layer_extent.yMinimum())
                        bot_right = crs_transform.transform(layer_extent.xMaximum(), layer_extent.yMaximum())
                        layer_extent = QgsRectangle(top_left, bot_right)

                    # Transform layer extent from map coordinates to canvas coordinates
                    top_left = coord_transform.transform(layer_extent.xMinimum(), layer_extent.yMinimum())
                    bot_right = coord_transform.transform(layer_extent.xMaximum(), layer_extent.yMaximum())
                    layer_rect = QgsRectangle(top_left.x(), top_left.y(), bot_right.x(), bot_right.y())

                    if canvas_rect.contains(layer_rect) or canvas_rect.intersects(layer_rect):
                        # At least a part of the layer is visible in canvas, check if pos is over the layer
                        # The check is made with a rectangle centered in pos, with a search radius.
                        radius = QgsMapTool.searchRadiusMU(self.canvas)  # search radius
                        p = coord_transform.toMapCoordinates(pos.x(), pos.y())
                        rect = QgsRectangle(p.x() - radius, p.y() - radius, p.x() + radius, p.y() + radius)

                        # Transform rect from canvas crs to layer crs
                        if layer_crs.authid() != canvas_crs.authid():
                            context = QgsProject.instance().transformContext()
                            crs_transform = QgsCoordinateTransform(canvas_crs, layer_crs, context)
                            top_left = crs_transform.transform(rect.xMinimum(), rect.yMinimum())
                            bot_right = crs_transform.transform(rect.xMaximum(), rect.yMaximum())
                            rect = QgsRectangle(top_left, bot_right)

                        request = QgsFeatureRequest()
                        request.setFilterRect(rect)
                        request.setFlags(QgsFeatureRequest.ExactIntersect)

                        it = layer.getFeatures(request)
                        if it.nextFeature(QgsFeature()):
                            # If pos is over the layer feature, return layer's name
                            layer_name = layer.sourceName()
                            break
            except Exception as e:
                # Just in case that the transformation can't be applied to the layer, pass
                # logger.debug("Error in the transformation. {} {}".format(layer_extent, e))
                pass

        return layer_name
