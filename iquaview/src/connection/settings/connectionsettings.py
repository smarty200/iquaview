"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to configure all the connections to operate the AUV.
 It includes the setup parameters for theconnections to the
 vehicle, gps, usbl and joystick.
"""
from importlib import util
from copy import copy
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal
from iquaview.src.ui.ui_connection_settings import Ui_connectionSettingsWidget


class ConnectionSettingsWidget(QWidget, Ui_connectionSettingsWidget):
    usbl_send_period_signal = pyqtSignal()

    def __init__(self, config, vehicle_info, parent=None):
        super(ConnectionSettingsWidget, self).__init__(parent)
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info

        # Widgets are promoted from QtDesigner
        # self.auv_connection_widget = AUVConnectionWidget(self)
        # self.gps_connection_widget = GPSConnectionWidget(self)
        # self.usbl_connection_widget = USBLConnectionWidget(self)

        self.load_settings()

    def load_settings(self):

        if util.find_spec('iquaview_evologics_usbl') is None:
            self.usbl_connection_widget.hide()

        """ Load data from vehicle_info and config file"""
        self.auv_connection_widget.ip = self.vehicle_info.get_vehicle_ip()
        self.auv_connection_widget.port = self.vehicle_info.get_vehicle_port()

        self.gps_connection_widget.heading_serialPortRadioButton.toggled.emit(self.config.csettings['gps_heading_serial'])
        if self.config.csettings['gps_heading_serial']:
            self.gps_connection_widget.heading_serialPortRadioButton.setChecked(True)
            self.gps_connection_widget.heading_TCPRadioButton.setChecked(False)
        else:
            self.gps_connection_widget.heading_serialPortRadioButton.setChecked(False)
            self.gps_connection_widget.heading_TCPRadioButton.setChecked(True)

        self.gps_connection_widget.configure_radioButton.setChecked(self.config.csettings['gps_configure_heading'])
        self.gps_connection_widget.same_radioButton.setChecked(not self.config.csettings['gps_configure_heading'])
        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.gps_connection_widget.usbl_imu_radioButton.setChecked(self.config.csettings['usbl_imu_heading_enable'])
        else:
            self.gps_connection_widget.usbl_imu_radioButton.setChecked(False)

        self.gps_connection_widget.serialPortRadioButton.toggled.emit(self.config.csettings['gps_serial'])
        if self.config.csettings['gps_serial']:
            self.gps_connection_widget.serialPortRadioButton.setChecked(True)
            self.gps_connection_widget.TCPRadioButton.setChecked(False)

        else:
            self.gps_connection_widget.serialPortRadioButton.setChecked(False)
            self.gps_connection_widget.TCPRadioButton.setChecked(True)

        self.gps_connection_widget.configure_heading = copy(self.config.csettings['gps_configure_heading'])
        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.gps_connection_widget.imu_heading = copy(self.config.csettings['usbl_imu_heading_enable'])
        self.gps_connection_widget.serial_port = copy(self.config.csettings['gps_serial_port'])
        self.gps_connection_widget.serial_baudrate = copy(self.config.csettings['gps_serial_baudrate'])
        self.gps_connection_widget.heading_serial_port = copy(self.config.csettings['gps_heading_serial_port'])
        self.gps_connection_widget.heading_serial_baudrate = copy(self.config.csettings['gps_heading_serial_baudrate'])

        self.gps_connection_widget.ip = copy(self.config.csettings['gps_ip'])
        self.gps_connection_widget.gga_port = copy(self.config.csettings['gps_gga_port'])
        self.gps_connection_widget.protocol = copy(self.config.csettings['gps_protocol'])

        self.gps_connection_widget.heading_ip = copy(self.config.csettings['gps_heading_ip'])
        self.gps_connection_widget.hdt_port = copy(self.config.csettings['gps_hdt_port'])
        self.gps_connection_widget.heading_protocol = copy(self.config.csettings['gps_heading_protocol'])

        self.gps_connection_widget.imu_port = copy(self.config.csettings['usbl_imu_port'])

        self.usbl_connection_widget.ip = copy(self.config.csettings['usbl_ip'])
        self.usbl_connection_widget.port = copy(self.config.csettings['usbl_port'])
        self.usbl_connection_widget.ownid = copy(self.config.csettings['usbl_own_id'])
        self.usbl_connection_widget.targetid = copy(self.config.csettings['usbl_target_id'])
        self.usbl_connection_widget.send_period = copy(self.config.csettings['usbl_send_period'])
        self.usbl_connection_widget.send_continuous = copy(self.config.csettings['usbl_send_continuous'])

        self.teleoperation_connection_widget.joystickdevice = self.config.csettings['joystick_device']

    def apply_settings(self):
        """ Apply settings"""

        if self.valid_parameters():
            self.vehicle_info.set_vehicle_ip(str(self.auv_connection_widget.ip))
            self.vehicle_info.set_vehicle_port(str(self.auv_connection_widget.port))
            self.config.csettings['gps_configure_heading'] = copy(self.gps_connection_widget.configure_heading)
            if util.find_spec('iquaview_evologics_usbl') is not None:
                self.config.csettings['usbl_imu_heading_enable'] = copy(self.gps_connection_widget.imu_heading)
                self.config.csettings['usbl_imu_port'] = copy(self.gps_connection_widget.imu_port)
            self.config.csettings['gps_serial'] = self.gps_connection_widget.is_serial()
            if self.config.csettings['gps_serial']:
                self.config.csettings['gps_serial_port'] = copy(self.gps_connection_widget.serial_port)
                self.config.csettings['gps_serial_baudrate'] = copy(self.gps_connection_widget.serial_baudrate)
            else:
                self.config.csettings['gps_ip'] = copy(self.gps_connection_widget.ip)
                self.config.csettings['gps_gga_port'] = copy(self.gps_connection_widget.gga_port)
                self.config.csettings['gps_protocol'] = copy(self.gps_connection_widget.protocol)

            self.config.csettings['gps_heading_serial'] = self.gps_connection_widget.heading_is_serial()
            if self.config.csettings['gps_heading_serial']:
                self.config.csettings['gps_heading_serial_port'] = copy(self.gps_connection_widget.heading_serial_port)
                self.config.csettings['gps_heading_serial_baudrate'] = copy(self.gps_connection_widget.heading_serial_baudrate)
            else:
                self.config.csettings['gps_heading_ip'] = copy(self.gps_connection_widget.heading_ip)
                self.config.csettings['gps_hdt_port'] = copy(self.gps_connection_widget.hdt_port)
                self.config.csettings['gps_heading_protocol'] = copy(self.gps_connection_widget.heading_protocol)

            self.config.csettings['usbl_ip'] = copy(self.usbl_connection_widget.ip)
            self.config.csettings['usbl_port'] = copy(self.usbl_connection_widget.port)
            self.config.csettings['usbl_own_id'] = copy(self.usbl_connection_widget.ownid)
            self.config.csettings['usbl_target_id'] = copy(self.usbl_connection_widget.targetid)
            self.config.csettings['usbl_send_period'] = copy(self.usbl_connection_widget.send_period)
            self.config.csettings['usbl_send_continuous'] = copy(self.usbl_connection_widget.send_continuous)
            self.config.csettings['joystick_device'] = copy(self.teleoperation_connection_widget.joystickdevice)

            self.usbl_send_period_signal.emit()


    def save(self):
        """ Save settings to config file"""
        if self.valid_parameters():
            self.vehicle_info.set_vehicle_ip(str(self.auv_connection_widget.ip))
            self.vehicle_info.set_vehicle_port(str(self.auv_connection_widget.port))
            self.config.settings['gps_configure_heading'] = copy(self.gps_connection_widget.configure_heading)
            if util.find_spec('iquaview_evologics_usbl') is not None:
                self.config.settings['usbl_imu_heading_enable'] = copy(self.gps_connection_widget.imu_heading)
                self.config.settings['usbl_imu_port'] = copy(self.gps_connection_widget.imu_port)
            self.config.settings['gps_serial'] = copy(self.gps_connection_widget.is_serial())
            if self.gps_connection_widget.serialPortRadioButton.isChecked():
                self.config.settings['gps_serial_port'] = copy(self.gps_connection_widget.serial_port)
                self.config.settings['gps_serial_baudrate'] = copy(self.gps_connection_widget.serial_baudrate)
            else:
                self.config.settings['gps_ip'] = copy(self.gps_connection_widget.ip)
                self.config.settings['gps_gga_port'] = copy(self.gps_connection_widget.gga_port)
                self.config.settings['gps_protocol'] = copy(self.gps_connection_widget.protocol)

            self.config.settings['gps_heading_serial'] = copy(self.gps_connection_widget.heading_is_serial())
            if self.config.settings['gps_heading_serial']:
                self.config.settings['gps_heading_serial_port'] = copy(self.gps_connection_widget.heading_serial_port)
                self.config.settings['gps_heading_serial_baudrate'] = copy(self.gps_connection_widget.heading_serial_baudrate)
            else:
                self.config.settings['gps_heading_ip'] = copy(self.gps_connection_widget.heading_ip)
                self.config.settings['gps_hdt_port'] = copy(self.gps_connection_widget.hdt_port)
                self.config.settings['gps_heading_protocol'] = copy(self.gps_connection_widget.heading_protocol)


            self.config.settings['usbl_ip'] = copy(self.usbl_connection_widget.ip)
            self.config.settings['usbl_port'] = copy(self.usbl_connection_widget.port)
            self.config.settings['usbl_own_id'] = copy(self.usbl_connection_widget.ownid)
            self.config.settings['usbl_target_id'] = copy(self.usbl_connection_widget.targetid)
            self.config.settings['usbl_send_period'] = copy(self.usbl_connection_widget.send_period)
            self.config.settings['usbl_send_continuous'] = copy(self.usbl_connection_widget.send_continuous)
            self.config.settings['joystick_device'] = copy(self.teleoperation_connection_widget.joystickdevice)

            self.vehicle_info.save()
            self.config.save()
            self.apply_settings()
            return True

        return False

    def valid_parameters(self):
        """ Check if parameters are valid"""
        return (self.auv_connection_widget.is_auv_valid() and
                self.gps_connection_widget.is_gps_valid() and
                self.usbl_connection_widget.is_usbl_valid())
