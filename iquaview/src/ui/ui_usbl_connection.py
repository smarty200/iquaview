# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_usbl_connection.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_USBLConnectionWidget(object):
    def setupUi(self, USBLConnectionWidget):
        USBLConnectionWidget.setObjectName("USBLConnectionWidget")
        USBLConnectionWidget.resize(482, 147)
        self.verticalLayout = QtWidgets.QVBoxLayout(USBLConnectionWidget)
        self.verticalLayout.setContentsMargins(0, -1, 0, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.usbl_groupBox = QtWidgets.QGroupBox(USBLConnectionWidget)
        self.usbl_groupBox.setMinimumSize(QtCore.QSize(318, 129))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.usbl_groupBox.setFont(font)
        self.usbl_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.usbl_groupBox.setFlat(False)
        self.usbl_groupBox.setObjectName("usbl_groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.usbl_groupBox)
        self.gridLayout.setContentsMargins(-1, 19, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.send_period_label = QtWidgets.QLabel(self.usbl_groupBox)
        self.send_period_label.setMinimumSize(QtCore.QSize(86, 0))
        self.send_period_label.setObjectName("send_period_label")
        self.gridLayout.addWidget(self.send_period_label, 2, 0, 1, 1)
        self.id_usbl_label = QtWidgets.QLabel(self.usbl_groupBox)
        self.id_usbl_label.setMinimumSize(QtCore.QSize(86, 0))
        self.id_usbl_label.setObjectName("id_usbl_label")
        self.gridLayout.addWidget(self.id_usbl_label, 1, 0, 1, 1)
        self.ip_usbl_label = QtWidgets.QLabel(self.usbl_groupBox)
        self.ip_usbl_label.setMinimumSize(QtCore.QSize(86, 0))
        self.ip_usbl_label.setObjectName("ip_usbl_label")
        self.gridLayout.addWidget(self.ip_usbl_label, 0, 0, 1, 1)
        self.port_usbl_label = QtWidgets.QLabel(self.usbl_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.port_usbl_label.sizePolicy().hasHeightForWidth())
        self.port_usbl_label.setSizePolicy(sizePolicy)
        self.port_usbl_label.setObjectName("port_usbl_label")
        self.gridLayout.addWidget(self.port_usbl_label, 0, 4, 1, 1)
        self.ip_usbl_text = QtWidgets.QLineEdit(self.usbl_groupBox)
        self.ip_usbl_text.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ip_usbl_text.sizePolicy().hasHeightForWidth())
        self.ip_usbl_text.setSizePolicy(sizePolicy)
        self.ip_usbl_text.setObjectName("ip_usbl_text")
        self.gridLayout.addWidget(self.ip_usbl_text, 0, 1, 1, 3)
        self.send_period_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.usbl_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.send_period_doubleSpinBox.sizePolicy().hasHeightForWidth())
        self.send_period_doubleSpinBox.setSizePolicy(sizePolicy)
        self.send_period_doubleSpinBox.setMinimumSize(QtCore.QSize(0, 0))
        self.send_period_doubleSpinBox.setMinimum(1.0)
        self.send_period_doubleSpinBox.setMaximum(100.0)
        self.send_period_doubleSpinBox.setSingleStep(0.5)
        self.send_period_doubleSpinBox.setProperty("value", 1.0)
        self.send_period_doubleSpinBox.setObjectName("send_period_doubleSpinBox")
        self.gridLayout.addWidget(self.send_period_doubleSpinBox, 2, 1, 1, 1)
        self.port_usbl_text = QtWidgets.QLineEdit(self.usbl_groupBox)
        self.port_usbl_text.setEnabled(True)
        self.port_usbl_text.setMaximumSize(QtCore.QSize(55, 16777215))
        self.port_usbl_text.setObjectName("port_usbl_text")
        self.gridLayout.addWidget(self.port_usbl_text, 0, 5, 1, 1)
        self.id_usbl_text = QtWidgets.QLineEdit(self.usbl_groupBox)
        self.id_usbl_text.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.id_usbl_text.sizePolicy().hasHeightForWidth())
        self.id_usbl_text.setSizePolicy(sizePolicy)
        self.id_usbl_text.setMaximumSize(QtCore.QSize(100, 16777215))
        self.id_usbl_text.setText("")
        self.id_usbl_text.setObjectName("id_usbl_text")
        self.gridLayout.addWidget(self.id_usbl_text, 1, 1, 1, 1)
        self.targetid_usbl_label = QtWidgets.QLabel(self.usbl_groupBox)
        self.targetid_usbl_label.setObjectName("targetid_usbl_label")
        self.gridLayout.addWidget(self.targetid_usbl_label, 1, 2, 1, 1)
        self.targetid_usbl_text = QtWidgets.QLineEdit(self.usbl_groupBox)
        self.targetid_usbl_text.setEnabled(True)
        self.targetid_usbl_text.setMaximumSize(QtCore.QSize(100, 16777215))
        self.targetid_usbl_text.setObjectName("targetid_usbl_text")
        self.gridLayout.addWidget(self.targetid_usbl_text, 1, 3, 1, 1)
        self.send_continuous_checkBox = QtWidgets.QCheckBox(self.usbl_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.send_continuous_checkBox.sizePolicy().hasHeightForWidth())
        self.send_continuous_checkBox.setSizePolicy(sizePolicy)
        self.send_continuous_checkBox.setChecked(False)
        self.send_continuous_checkBox.setObjectName("send_continuous_checkBox")
        self.gridLayout.addWidget(self.send_continuous_checkBox, 2, 2, 1, 2)
        self.verticalLayout.addWidget(self.usbl_groupBox)

        self.retranslateUi(USBLConnectionWidget)
        self.send_continuous_checkBox.toggled['bool'].connect(self.send_period_doubleSpinBox.setDisabled)
        self.send_continuous_checkBox.toggled['bool'].connect(self.send_period_label.setDisabled)
        QtCore.QMetaObject.connectSlotsByName(USBLConnectionWidget)

    def retranslateUi(self, USBLConnectionWidget):
        _translate = QtCore.QCoreApplication.translate
        USBLConnectionWidget.setWindowTitle(_translate("USBLConnectionWidget", "USBL Connection"))
        self.usbl_groupBox.setTitle(_translate("USBLConnectionWidget", "USBL Connection"))
        self.send_period_label.setText(_translate("USBLConnectionWidget", "Send period:"))
        self.id_usbl_label.setText(_translate("USBLConnectionWidget", "USBL Id:"))
        self.ip_usbl_label.setText(_translate("USBLConnectionWidget", "IP:"))
        self.port_usbl_label.setText(_translate("USBLConnectionWidget", "Port:"))
        self.send_period_doubleSpinBox.setSuffix(_translate("USBLConnectionWidget", " s"))
        self.targetid_usbl_label.setText(_translate("USBLConnectionWidget", "Target Id:"))
        self.send_continuous_checkBox.setText(_translate("USBLConnectionWidget", "Continuous"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    USBLConnectionWidget = QtWidgets.QWidget()
    ui = Ui_USBLConnectionWidget()
    ui.setupUi(USBLConnectionWidget)
    USBLConnectionWidget.show()
    sys.exit(app.exec_())

