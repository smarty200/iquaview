# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_dataoutputmanager.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DataOutputManagerWidget(object):
    def setupUi(self, DataOutputManagerWidget):
        DataOutputManagerWidget.setObjectName("DataOutputManagerWidget")
        DataOutputManagerWidget.resize(400, 300)
        self.gridLayout = QtWidgets.QGridLayout(DataOutputManagerWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.new_connection_pushButton = QtWidgets.QPushButton(DataOutputManagerWidget)
        self.new_connection_pushButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.new_connection_pushButton.sizePolicy().hasHeightForWidth())
        self.new_connection_pushButton.setSizePolicy(sizePolicy)
        self.new_connection_pushButton.setMaximumSize(QtCore.QSize(125, 25))
        self.new_connection_pushButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.new_connection_pushButton.setObjectName("new_connection_pushButton")
        self.gridLayout.addWidget(self.new_connection_pushButton, 0, 0, 1, 1)
        self.scrollArea = QtWidgets.QScrollArea(DataOutputManagerWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 380, 249))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout.addWidget(self.scrollArea, 1, 0, 1, 1)

        self.retranslateUi(DataOutputManagerWidget)
        QtCore.QMetaObject.connectSlotsByName(DataOutputManagerWidget)

    def retranslateUi(self, DataOutputManagerWidget):
        _translate = QtCore.QCoreApplication.translate
        DataOutputManagerWidget.setWindowTitle(_translate("DataOutputManagerWidget", "Form"))
        self.new_connection_pushButton.setText(_translate("DataOutputManagerWidget", "Add Connection"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DataOutputManagerWidget = QtWidgets.QWidget()
    ui = Ui_DataOutputManagerWidget()
    ui.setupUi(DataOutputManagerWidget)
    DataOutputManagerWidget.show()
    sys.exit(app.exec_())

