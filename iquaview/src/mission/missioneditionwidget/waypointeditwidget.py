"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to handle the editing of a clicked waypoint,
 allowing to change coordinates, depth/altitude, controller type
 and actions that should be performed at each waypoint.
"""

import logging
from math import degrees, radians

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QValidator
from PyQt5.QtWidgets import QWidget, QListWidgetItem, QDialog, QLineEdit, QDoubleSpinBox

from iquaview_lib.cola2api.mission_types import (GOTO_MANEUVER,
                                                 SECTION_MANEUVER,
                                                 PARK_MANEUVER,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionPark,
                                                 MissionAction,
                                                 MissionSection)
from iquaview.src.mission.missioneditionwidget.loadaddactiondialog import Loadaddactiondialog
from iquaview.src.ui.ui_waypoint_edit import Ui_WaypointEditWidget
from iquaview_lib.utils.textvalidator import validate_custom_double, get_color

PARK_DISABLED = 0
PARK_ANCHOR = 1
PARK_HOLONOMIC = 2

logger = logging.getLogger(__name__)


class WaypointEditWidget(QWidget, Ui_WaypointEditWidget):
    control_state_signal = pyqtSignal(bool)

    def __init__(self, config, mission_track, vehicle_namespace, multiple_edition=False, parent=None):
        super(WaypointEditWidget, self).__init__(parent)
        self.setupUi(self)
        self.displaying = False
        self.mission_track = mission_track
        self.config = config
        self.vehicle_namespace = vehicle_namespace
        self.multiple_edition = multiple_edition
        self.heave_mode_widget.set_multiple_edition(multiple_edition)
        self.heave_mode_widget.set_parent_widget(self)

        if multiple_edition:
            self.previousWpButton.setEnabled(False)
            self.previousWpButton.hide()
            self.nextWpButton.setEnabled(False)
            self.nextWpButton.hide()
            # self.addAction_pushButton.setEnabled(False)
            self.removeAction_pushButton.setEnabled(False)

        self.heave_mode_widget.heave_mode_changed_signal.connect(self.apply_changes)
        self.delete_pushButton.clicked.connect(self.on_click_remove)
        self.no_altitude_comboBox.currentIndexChanged.connect(self.on_no_altitude_box_changed)
        self.park_state_comboBox.currentIndexChanged.connect(self.on_park_state_box_changed)
        self.latitude_lineEdit.textChanged.connect(self.on_text_changed)
        self.longitude_lineEdit.textChanged.connect(self.on_text_changed)
        self.parktime_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.park_yaw_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.speed_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.tolerance_xy_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.parktime_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.park_yaw_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.speed_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.tolerance_xy_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.removeAction_pushButton.clicked.connect(self.rm_action)
        self.addAction_pushButton.clicked.connect(self.add_action)
        self.previousWpButton.clicked.connect(self.load_previous_wp)
        self.nextWpButton.clicked.connect(self.load_next_wp)

        self.show_step = -1
        self.step_list = list()
        self.mCtrl = False

        self.heave_mode_widget.on_heave_mode_box_changed(0)
        self.on_park_state_box_changed(0)

        if not multiple_edition:
            self.load_next_wp()
            self.mission_track.mission_changed.connect(self.show_mission_step)

    def get_step_list(self):
        """Return the current step list"""
        return self.step_list

    def on_text_changed(self, string):

        sender = self.sender()

        state = validate_custom_double(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

        if state == QValidator.Acceptable:
            if self.heave_mode_widget.is_valid_altitude():
                self.apply_changes()
            else:
                sender.undo()

    def on_spinbox_changed(self, value=0):
        sender = self.sender()
        state = self.evaluate_spinbox(sender, str(sender.text()))

        if state == QValidator.Acceptable:

            if self.heave_mode_widget.is_valid_altitude():
                self.apply_changes()

    def evaluate_spinbox(self, spinbox: QDoubleSpinBox, value_text):
        """
        Set spinbox stylesheet depending if value_text is valid
        :param spinbox: spinbox
        :type spinbox: QDouubleSpinBox
        :param value_text: value to evaluate
        :type value_text: str
        :return: qvalidator state
        :rtype: int
        """
        state = validate_custom_double(str(value_text))
        color = get_color(state)
        spinbox.setStyleSheet('QDoubleSpinBox { background-color: %s }' % color)
        return state

    def is_custom_double_acceptable(self, text):
        return validate_custom_double(text) == QValidator.Acceptable

    def check_and_set_lineedit_value(self, new_value: bool, lineedit: QLineEdit, value):
        """
        If new_value is True set value on lineedit, otherwise set empty string
        :param new_value: If new_value is True set value on lineedit, otherwise set empty string
        :type new_value: bool
        :param lineedit: the lineedit to be edited
        :type lineedit: QLineEdit
        :param value: value to set
        :type value: str
        """
        if new_value:
            self.set_lineedit_value(lineedit, value)
        else:
            lineedit.setText("")

    def check_and_set_spinbox_value(self, new_value: bool, spinbox: QDoubleSpinBox, value):
        """
        If new_value is True set value on spinbox, otherwise set empty value
        :param new_value: If new_value is True set value on spinbox, otherwise set empty value
        :type new_value: bool
        :param spinbox: the spinbox to be edited
        :type spinbox: QDoubleSpinBox
        :param value: value to set
        :type value: str
        """
        if new_value:
            spinbox.setValue(value)
        else:
            spinbox.clear()
        state = validate_custom_double(str(spinbox.text()))
        color = get_color(state)
        spinbox.setStyleSheet('QDoubleSpinBox { background-color: %s }' % color)

    def set_lineedit_value(self, lineedit: QLineEdit, value):
        """
        Set value on lineedit
        :param lineedit: the lineedit to be edited
        :type lineedit: QLineEdit
        :param value: value to set
        :type value: float
        """
        if not lineedit.text() or lineedit.text() != value:
            if float(value).is_integer() and float(value) != 0.0:
                lineedit.setText(str(int(float(value))))
            else:
                lineedit.setText(str(value))

    def on_click_remove(self):
        if self.multiple_edition:
            for step in reversed(self.step_list):
                self.mission_track.remove_step(step)
            self.show_empty_mission_step()

        else:
            if self.show_step >= 0:
                self.mission_track.remove_step(self.show_step)

    # on click save current wp
    def apply_changes(self):
        mission_step_list = []
        id_list = []

        if not self.multiple_edition:
            self.step_list = list()
            if self.show_step >= 0:
                self.step_list.append(self.show_step)

        for step in reversed(self.step_list):

            logger.debug(
                "Save: Mission has {} steps. Im going to update step {}".format(self.mission_track.get_mission_length(),
                                                                                step))

            if self.is_custom_double_acceptable(self.latitude_lineEdit.text()):
                latitude = float(self.latitude_lineEdit.text())
            else:
                latitude = self.mission_track.get_step(step).get_maneuver().final_latitude

            if self.is_custom_double_acceptable(self.longitude_lineEdit.text()):
                longitude = float(self.longitude_lineEdit.text())
            else:
                longitude = self.mission_track.get_step(step).get_maneuver().final_longitude

            if ((self.heave_mode_widget.is_heave_mode_depth()
                 or self.heave_mode_widget.is_heave_mode_both())
                    and self.is_custom_double_acceptable(str(self.heave_mode_widget.depth_doubleSpinBox.text()))):
                depth = self.heave_mode_widget.get_depth()
            else:
                depth = self.mission_track.get_step(step).get_maneuver().final_depth

            if ((self.heave_mode_widget.is_heave_mode_altitude()
                 or self.heave_mode_widget.is_heave_mode_both())
                    and self.is_custom_double_acceptable(str(self.heave_mode_widget.altitude_doubleSpinBox.text()))):
                altitude = self.heave_mode_widget.get_altitude()
            else:
                altitude = self.mission_track.get_step(step).get_maneuver().final_altitude

            if self.is_custom_double_acceptable(str(self.tolerance_xy_doubleSpinBox.text())):
                tolerance = self.tolerance_xy_doubleSpinBox.value()
            else:
                tolerance = self.mission_track.get_step(step).get_maneuver().tolerance_xy

            if self.heave_mode_widget.heave_mode_comboBox.count() < 4:
                heave_mode = self.heave_mode_widget.get_heave_mode()
            else:
                heave_mode = self.mission_track.get_step(step).get_maneuver().heave_mode

            if self.no_altitude_comboBox.count() < 3:
                no_altitude = self.no_altitude_comboBox.currentText() == "Go up"
            else:
                no_altitude = self.mission_track.get_step(step).get_maneuver().no_altitude_goes_up

            if self.is_custom_double_acceptable(str(self.speed_doubleSpinBox.text())):
                speed = float(self.speed_doubleSpinBox.value())
            else:
                speed = self.mission_track.get_step(step).get_maneuver().surge_velocity

            if ((self.park_state_comboBox.currentIndex() == PARK_ANCHOR
                 or self.park_state_comboBox.currentIndex() == PARK_HOLONOMIC)
                    and self.is_custom_double_acceptable(str(self.parktime_doubleSpinBox.text()))):
                park_time = float(self.parktime_doubleSpinBox.value())

            elif self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER:
                park_time = self.mission_track.get_step(step).get_maneuver().time
            else:
                park_time = 10

            if (self.park_state_comboBox.currentIndex() == PARK_HOLONOMIC
                    and self.is_custom_double_acceptable(str(self.park_yaw_doubleSpinBox.text()))):
                yaw = radians(float(self.park_yaw_doubleSpinBox.value()))
                use_yaw = True
            elif (self.park_state_comboBox.count() == 4
                  and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER
                  and self.mission_track.get_step(step).get_maneuver().use_yaw is True):
                yaw = self.mission_track.get_step(step).get_maneuver().final_yaw
                use_yaw = True
            else:
                yaw = 0
                use_yaw = False

            mission_step = MissionStep()
            if ((self.park_state_comboBox.currentIndex() == PARK_ANCHOR
                 or self.park_state_comboBox.currentIndex() == PARK_HOLONOMIC)
                    or (self.park_state_comboBox.count() == 4
                        and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER)):
                logger.debug("park")
                park = MissionPark(latitude, longitude, depth, altitude,
                                   yaw, use_yaw,
                                   heave_mode, speed, park_time, no_altitude)
                mission_step.add_maneuver(park)

            elif (step == 0 and self.park_state_comboBox.currentIndex() == PARK_DISABLED
                  or (self.park_state_comboBox.count() == 4
                      and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == GOTO_MANEUVER)):
                logger.debug("goto")
                goto = MissionGoto(latitude, longitude, depth, altitude, heave_mode, speed, tolerance, no_altitude)
                mission_step.add_maneuver(goto)

            else:  # self.park_state_comboBox.currentIndex() == PARK_DISABLED:
                logger.debug("section")
                # if previous is in the list, get z from list, otherwsie get z from track
                previous_latitude = self.mission_track.get_step(step - 1).get_maneuver().final_latitude
                previous_longitude = self.mission_track.get_step(step - 1).get_maneuver().final_longitude
                if (step - 1) in self.step_list:
                    previous_depth = self.heave_mode_widget.get_depth()
                else:
                    previous_depth = self.mission_track.get_step(step - 1).get_maneuver().final_depth

                sec = MissionSection(previous_latitude, previous_longitude, previous_depth,
                                     latitude, longitude, depth, altitude, heave_mode,
                                     speed, tolerance, no_altitude)
                mission_step.add_maneuver(sec)

            if ((step + 1) <= (self.mission_track.get_mission_length() - 1)
                    and self.mission_track.get_step(step + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
                self.mission_track.get_step(step + 1).get_maneuver().initial_depth = depth

            # copy actions to updated step
            mission_step.actions = self.mission_track.get_step(step).get_actions()
            mission_step_list.append(mission_step)
            id_list.append(step)

        self.mission_track.update_steps(id_list, mission_step_list)

    def on_no_altitude_box_changed(self, i):
        if i < 2:
            # remove '-' item
            if self.no_altitude_comboBox.count() == 3:
                self.no_altitude_comboBox.removeItem(2)

            if self.heave_mode_widget.is_valid_altitude():
                self.apply_changes()

    def on_park_state_box_changed(self, i):

        if i < 3:
            # remove '-' item
            if self.park_state_comboBox.count() == 4:
                self.park_state_comboBox.removeItem(3)

            if self.park_state_comboBox.currentIndex() == PARK_DISABLED:  # disabled
                self.parkTime_label.setVisible(False)
                self.parktime_doubleSpinBox.setVisible(False)
                self.yaw_label.setVisible(False)
                self.park_yaw_doubleSpinBox.setVisible(False)
                self.tolerance_label.setVisible(True)
                self.tolerance_xy_doubleSpinBox.setVisible(True)
            elif self.park_state_comboBox.currentIndex() == PARK_ANCHOR:  # Anchor
                self.parkTime_label.setVisible(True)
                self.parktime_doubleSpinBox.setVisible(True)
                self.yaw_label.setVisible(False)
                self.park_yaw_doubleSpinBox.setVisible(False)
                self.tolerance_label.setVisible(False)
                self.tolerance_xy_doubleSpinBox.setVisible(False)
            elif self.park_state_comboBox.currentIndex() == PARK_HOLONOMIC:  # Holonomic
                self.parkTime_label.setVisible(True)
                self.parktime_doubleSpinBox.setVisible(True)
                self.yaw_label.setVisible(True)
                self.park_yaw_doubleSpinBox.setVisible(True)
                self.tolerance_label.setVisible(False)
                self.tolerance_xy_doubleSpinBox.setVisible(False)

            if self.heave_mode_widget.is_valid_altitude():
                self.apply_changes()

        else:
            self.parkTime_label.setVisible(False)
            self.parktime_doubleSpinBox.setVisible(False)
            self.yaw_label.setVisible(False)
            self.park_yaw_doubleSpinBox.setVisible(False)
            self.tolerance_label.setVisible(True)
            self.tolerance_xy_doubleSpinBox.setVisible(True)

    # add new action
    def add_action(self):
        load_dialog = Loadaddactiondialog(self.config)
        result = load_dialog.exec_()
        if result == QDialog.Accepted:

            for wp in self.step_list:

                mission_action = MissionAction(self.vehicle_namespace + load_dialog.get_action_id(),
                                               load_dialog.get_params())

                logger.debug(mission_action)
                self.mission_track.get_step(wp).add_action(mission_action)

            if self.multiple_edition:
                self.show_multiple_features(self.step_list)
            else:
                self.show_mission_step(self.show_step)

        self.apply_changes()

    # remove action
    def rm_action(self):
        logger.debug(self.show_step)
        if self.action_listWidget.currentItem() is not None:
            self.mission_track.get_step(self.show_step).remove_action(self.action_listWidget.currentItem().data(Qt.UserRole))
        self.show_mission_step(self.show_step)

        self.apply_changes()

    def show_mission_step(self, wp):
        if not self.displaying:
            self.displaying = True
            logger.debug("Showing mission with len: {},  step: {} ".format(self.mission_track.get_mission_length(), wp))
            if wp != -1:

                self.show_step = wp

                self.TitleWaypointLabel.setText("Waypoint {}".format(self.show_step + 1))
                mission_step = self.mission_track.get_step(self.show_step)
                maneuver = mission_step.get_maneuver()
                if maneuver.get_maneuver_type() == PARK_MANEUVER:  # for Park
                    if maneuver.use_yaw:
                        self.park_state_comboBox.setCurrentIndex(2)
                        self.check_and_set_spinbox_value(True,
                                                         self.park_yaw_doubleSpinBox,
                                                         degrees(maneuver.final_yaw))
                    else:
                        self.park_state_comboBox.setCurrentIndex(1)
                    self.check_and_set_spinbox_value(True,
                                                     self.parktime_doubleSpinBox,
                                                     maneuver.time)
                else:
                    self.park_state_comboBox.setCurrentIndex(0)
                self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(maneuver.heave_mode)

                # latitude
                self.check_and_set_spinbox_value(True,
                                                 self.speed_doubleSpinBox,
                                                 maneuver.surge_velocity)
                self.set_lineedit_value(self.latitude_lineEdit, maneuver.final_latitude)
                self.set_lineedit_value(self.longitude_lineEdit, maneuver.final_longitude)
                self.check_and_set_spinbox_value(True,
                                                 self.heave_mode_widget.depth_doubleSpinBox,
                                                 maneuver.final_depth)
                self.check_and_set_spinbox_value(True,
                                                 self.heave_mode_widget.altitude_doubleSpinBox,
                                                 maneuver.final_altitude)
                self.check_and_set_spinbox_value(True,
                                                 self.tolerance_xy_doubleSpinBox,
                                                 maneuver.tolerance_xy)

                goes_up = 0 if maneuver.no_altitude_goes_up else 1
                self.no_altitude_comboBox.setCurrentIndex(goes_up)

                self.action_listWidget.clear()
                actions = mission_step.get_actions()
                for a in actions:
                    action = QListWidgetItem()
                    text = a.get_action_id()
                    for param in a.get_parameters():
                        text += ", " + param.value
                    action.setText(text)
                    action.setData(Qt.UserRole, a)
                    self.action_listWidget.addItem(action)

            else:
                self.show_empty_mission_step()

            self.displaying = False

    def show_empty_mission_step(self):
        self.show_step = -1
        self.step_list = list()
        self.TitleWaypointLabel.setText("Mission is empty")
        #
        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(HEAVE_MODE_DEPTH)
        self.park_state_comboBox.setCurrentIndex(0)
        self.no_altitude_comboBox.setCurrentIndex(0)

        self.speed_doubleSpinBox.clear()
        self.parktime_doubleSpinBox.clear()
        self.park_yaw_doubleSpinBox.clear()
        self.heave_mode_widget.clear()
        self.tolerance_xy_doubleSpinBox.clear()

        self.evaluate_spinbox(self.speed_doubleSpinBox, "")
        self.evaluate_spinbox(self.parktime_doubleSpinBox, "")
        self.evaluate_spinbox(self.park_yaw_doubleSpinBox, "")
        self.evaluate_spinbox(self.heave_mode_widget.depth_doubleSpinBox, "")
        self.evaluate_spinbox(self.heave_mode_widget.altitude_doubleSpinBox, "")
        self.evaluate_spinbox(self.tolerance_xy_doubleSpinBox, "")

        self.latitude_lineEdit.setText("")
        self.longitude_lineEdit.setText("")

        self.action_listWidget.clear()

    def show_multiple_features(self, step_list):
        self.show_empty_mission_step()
        logger.debug("step list: " + str(step_list))
        if step_list:
            step_list.sort()
            self.step_list = step_list
            # if only have one step
            if len(step_list) == 1:
                self.show_mission_step(step_list[0])
            # more than one step
            else:
                # set title
                title = self.create_title(step_list)
                self.TitleWaypointLabel.setText(title)

                same_lat = True
                same_lon = True
                same_heave_mode = True
                same_depth = True
                same_altitude = True
                same_no_altitude = True
                same_park_time = True
                same_park_yaw = True
                same_speed = True
                same_tolerance_xy = True
                same_maneuver = True
                same_action = True
                same_actions = list()

                #         self.same_altitude = True

                # every list step
                for i in range(0, len(step_list)):
                    self.show_step = step_list[i]
                    mission_step = self.mission_track.get_step(self.show_step)
                    maneuver = mission_step.get_maneuver()

                    j = i + 1
                    while 0 <= j < len(step_list):
                        wp = step_list[j]
                        mission_step_two = self.mission_track.get_step(wp)
                        maneuver_two = mission_step_two.get_maneuver()

                        latitude = maneuver.final_latitude
                        longitude = maneuver.final_longitude

                        latitude_two = maneuver_two.final_latitude
                        longitude_two = maneuver_two.final_longitude

                        if float(latitude) != float(latitude_two):
                            same_lat = False

                        if float(longitude) != float(longitude_two):
                            same_lon = False

                        if maneuver.heave_mode != maneuver_two.heave_mode:
                            same_heave_mode = False

                        if ((maneuver.heave_mode == HEAVE_MODE_DEPTH
                             or maneuver.heave_mode == HEAVE_MODE_BOTH)
                                and (maneuver.heave_mode == HEAVE_MODE_DEPTH
                                     or maneuver.heave_mode == HEAVE_MODE_BOTH)):
                            if maneuver.final_depth != maneuver_two.final_depth:
                                same_depth = False

                        if ((maneuver.heave_mode == HEAVE_MODE_ALTITUDE
                             or maneuver.heave_mode == HEAVE_MODE_BOTH)
                                and (maneuver.heave_mode == HEAVE_MODE_ALTITUDE
                                     or maneuver.heave_mode == HEAVE_MODE_BOTH)):
                            if maneuver.final_altitude != maneuver_two.final_altitude:
                                same_altitude = False

                        if maneuver.no_altitude_goes_up != maneuver_two.no_altitude_goes_up:
                            same_no_altitude = False

                        if (maneuver.get_maneuver_type() == maneuver_two.get_maneuver_type()
                                and maneuver.get_maneuver_type() == PARK_MANEUVER):
                            # maneuver is PARK
                            if float(maneuver.time) != float(maneuver_two.time):
                                same_park_time = False

                            if maneuver.use_yaw and maneuver_two.use_yaw:
                                if float(maneuver.final_yaw) != float(maneuver_two.final_yaw):
                                    same_park_yaw = False

                        elif maneuver.get_maneuver_type() == PARK_MANEUVER or maneuver_two.get_maneuver_type() == PARK_MANEUVER:
                            same_maneuver = False

                        if float(maneuver.surge_velocity) != float(maneuver_two.surge_velocity):
                            same_speed = False

                        if float(maneuver.tolerance_xy) != float(maneuver_two.tolerance_xy):
                            same_tolerance_xy = False

                        if not same_actions:
                            for action in mission_step.actions:
                                same_actions.append(action)

                        for action in same_actions.copy():
                            found = False
                            for action_two in mission_step_two.actions:
                                if action.action_id == action_two.action_id:
                                    found = True
                            if not found:
                                for idx, action_copy in enumerate(same_actions.copy()):
                                    if action.action_id == action_copy.action_id:
                                        same_actions.remove(same_actions[idx])

                        if not mission_step.actions or not mission_step_two.actions or not same_action:
                            same_action = False
                            same_actions = list()

                        j += 1

                self.show_step = step_list[0]
                mission_step = self.mission_track.get_step(self.show_step)
                maneuver = mission_step.get_maneuver()

                if same_no_altitude:
                    if maneuver.no_altitude_goes_up:
                        self.no_altitude_comboBox.setCurrentIndex(0)
                    else:
                        self.no_altitude_comboBox.setCurrentIndex(1)
                elif self.no_altitude_comboBox.count() < 3:
                    self.no_altitude_comboBox.addItem("-")
                    self.no_altitude_comboBox.setCurrentIndex(2)

                if same_maneuver:
                    if maneuver.get_maneuver_type() == GOTO_MANEUVER:  # for Waypoint
                        self.park_state_comboBox.setCurrentIndex(0)

                    if maneuver.get_maneuver_type() == SECTION_MANEUVER:  # for a Section
                        self.park_state_comboBox.setCurrentIndex(0)

                    elif maneuver.get_maneuver_type() == PARK_MANEUVER:  # for Park
                        if maneuver.use_yaw:
                            self.park_state_comboBox.setCurrentIndex(2)

                            self.check_and_set_spinbox_value(same_park_yaw,
                                                             self.park_yaw_doubleSpinBox,
                                                             degrees(maneuver.final_yaw))

                        else:
                            self.park_state_comboBox.setCurrentIndex(1)

                        self.check_and_set_spinbox_value(same_park_time,
                                                         self.parktime_doubleSpinBox,
                                                         maneuver.time)

                elif self.park_state_comboBox.count() < 4:
                    self.park_state_comboBox.addItem("-")
                    self.park_state_comboBox.setCurrentIndex(3)

                if same_heave_mode:
                    if maneuver.heave_mode == HEAVE_MODE_DEPTH:
                        self.check_and_set_spinbox_value(same_depth,
                                                         self.heave_mode_widget.depth_doubleSpinBox,
                                                         maneuver.final_depth)
                        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(0)


                    elif maneuver.heave_mode == HEAVE_MODE_ALTITUDE:

                        self.check_and_set_spinbox_value(same_altitude,
                                                         self.heave_mode_widget.altitude_doubleSpinBox,
                                                         maneuver.final_altitude)

                        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(1)


                    elif maneuver.heave_mode == HEAVE_MODE_BOTH:

                        self.check_and_set_spinbox_value(same_depth,
                                                         self.heave_mode_widget.depth_doubleSpinBox,
                                                         maneuver.final_depth)

                        self.check_and_set_spinbox_value(same_altitude,
                                                         self.heave_mode_widget.altitude_doubleSpinBox,
                                                         maneuver.final_altitude)
                        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(2)

                elif self.heave_mode_widget.heave_mode_comboBox.count() < 4:
                    self.heave_mode_widget.heave_mode_comboBox.addItem("-")
                    self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(3)

                self.check_and_set_lineedit_value(same_lat,
                                                  self.latitude_lineEdit,
                                                  str(latitude))

                self.check_and_set_lineedit_value(same_lon,
                                                  self.longitude_lineEdit,
                                                  str(longitude))

                self.check_and_set_spinbox_value(same_depth,
                                                 self.heave_mode_widget.depth_doubleSpinBox,
                                                 maneuver.final_depth)

                self.check_and_set_spinbox_value(same_altitude,
                                                 self.heave_mode_widget.altitude_doubleSpinBox,
                                                 maneuver.final_altitude)

                self.check_and_set_spinbox_value(same_speed,
                                                 self.speed_doubleSpinBox,
                                                 maneuver.surge_velocity)

                self.check_and_set_spinbox_value(same_tolerance_xy,
                                                 self.tolerance_xy_doubleSpinBox,
                                                 maneuver.tolerance_xy)

                self.action_listWidget.clear()
                for a in same_actions:
                    action = QListWidgetItem()
                    text = a.get_action_id()
                    for param in a.get_parameters():
                        text += ", " + param.value
                    action.setText(text)
                    action.setData(Qt.UserRole, a)
                    self.action_listWidget.addItem(action)

        else:
            self.show_empty_mission_step()

    # load previous waypoint
    def load_previous_wp(self):
        length = self.mission_track.get_mission_length()
        if length > 0:
            if self.show_step > 0:
                # switch to previous wp
                self.show_mission_step(self.show_step - 1)
            else:
                # if current wp is 0 switch to last wp
                last = length - 1
                self.show_mission_step(last)
        else:
            self.show_empty_mission_step()

    # load next waypoint
    def load_next_wp(self):
        length = self.mission_track.get_mission_length()
        if length > 0:
            if self.show_step < (length - 1):
                # switch to next wp
                self.show_mission_step(self.show_step + 1)
            else:
                # if current wp is the last switch to first wp
                self.show_mission_step(0)
        else:
            self.show_empty_mission_step()

    def create_title(self, step_list):
        """
        Creates a title with a step list and returns it. In case of the title not fitting in the available space,
        it gets shortened and a tooltip is created to show all steps in step list
        """
        # Check available space
        max_width = self.scrollArea.width() - 50
        ppchar = 6.75  # pixels per character aprox
        limit_achieved = False
        title = "Waypoints: "

        if len(step_list) == 1:
            title = "Waypoint {}".format(step_list[0] + 1)
        else:
            # write wp selected
            for wp in step_list:
                s = str(wp + 1)
                if wp != step_list[-1]:
                    s += ","
                # limit maximum length of the title
                if (len(title) * ppchar + len(s) * ppchar) > max_width:
                    limit_achieved = True
                    break
                title += s

        tool_tip_text = ""
        if limit_achieved:  # Rewrite title accounting for less space due to the " ...+xx" at the end
            max_width = max_width - 75
            title = "Waypoints: "
            count = 0
            for wp in step_list:
                s = str(wp + 1) + ","
                if (len(title) * ppchar + len(s) * ppchar) > max_width:
                    title += " ...+" + str(len(step_list) - count) + " points"
                    break
                title += s
                count += 1

            #  Create ToolTip text
            cont = 0
            for wp in step_list:
                tool_tip_text += str(wp + 1)
                if wp != step_list[-1]:
                    tool_tip_text += ", "
                cont += 1
                if cont % 10 == 0 and wp < len(step_list) - 1:
                    tool_tip_text += "\n"

        self.TitleWaypointLabel.setToolTip(tool_tip_text)

        return title

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.mCtrl = True
            self.control_state_signal.emit(True)

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.mCtrl = False
            self.control_state_signal.emit(False)

    def resizeEvent(self, event):
        title = self.create_title(self.step_list)
        self.TitleWaypointLabel.setText(title)
