"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to show information about waypoints of the mission
"""

import math

from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QComboBox, QTextEdit, QSizePolicy
from PyQt5.QtCore import Qt, pyqtSignal, QTimer, QPoint
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.ui.ui_mission_table_dialog import Ui_MissionTableDialog
from iquaview_lib.cola2api.mission_types import (GOTO_MANEUVER,
                                                 SECTION_MANEUVER,
                                                 PARK_MANEUVER,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_BOTH,
                                                 HEAVE_MODE_ALTITUDE)


class MissionTableDialog(QDialog, Ui_MissionTableDialog):

    def __init__(self, mission_track : MissionTrack, parent=None):
        super(MissionTableDialog, self).__init__(parent)
        self.setupUi(self)
        self.mission_track = mission_track
        # for every column and row resize to contents

        for i in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        for i in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        self.reload()

    def reload(self):
        self.tableWidget.setRowCount(0)
        # self.tableWidget.clear()
        for i in range(self.mission_track.get_mission_length()):
            step = self.mission_track.get_step(i)
            self.tableWidget.insertRow(i)

            item = QTableWidgetItem()
            item.setFlags(Qt.ItemIsEnabled) # only read
            item.setText(str(step.get_maneuver().final_latitude))
            self.tableWidget.setItem(i, 0, item)

            item = QTableWidgetItem()
            item.setFlags(Qt.ItemIsEnabled) # only read
            item.setText(str(step.get_maneuver().final_longitude))
            self.tableWidget.setItem(i, 1, item)

            if step.get_maneuver().heave_mode == HEAVE_MODE_DEPTH:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("DEPTH")
                self.tableWidget.setItem(i, 2, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText(str(step.get_maneuver().final_depth))
                self.tableWidget.setItem(i, 3, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("")
                self.tableWidget.setItem(i, 4, item)

            if step.get_maneuver().heave_mode == HEAVE_MODE_ALTITUDE:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("ALTITUDE")
                self.tableWidget.setItem(i, 2, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("")
                self.tableWidget.setItem(i, 3, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText(str(step.get_maneuver().final_altitude))
                self.tableWidget.setItem(i, 4, item)

            elif step.get_maneuver().heave_mode == HEAVE_MODE_BOTH:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("BOTH")
                self.tableWidget.setItem(i, 2, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText(str(step.get_maneuver().final_depth))
                self.tableWidget.setItem(i, 3, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText(str(step.get_maneuver().final_altitude))
                self.tableWidget.setItem(i, 4, item)

            if step.get_maneuver().no_altitude_goes_up:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled) # only read
                item.setText("GO UP")
                # combobox = QComboBox()
                # combobox.addItem("Go up")
                # combobox.addItem("Ignore")
                # combobox.setCurrentIndex(0)
                # self.tableWidget.setCellWidget(i, 5, combobox)
                self.tableWidget.setItem(i, 5, item)
            else:
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("IGNORE")
                # combobox = QComboBox()
                # combobox.addItem("Go up")
                # combobox.addItem("Ignore")
                # combobox.setCurrentIndex(0)
                # self.tableWidget.setCellWidget(i, 5, combobox)
                self.tableWidget.setItem(i, 5, item)

            if (step.get_maneuver().get_maneuver_type() == GOTO_MANEUVER
                    or step.get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("DISABLED")
                self.tableWidget.setItem(i, 6, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("")
                self.tableWidget.setItem(i, 7, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText("")
                self.tableWidget.setItem(i, 8, item)

                item = QTableWidgetItem()
                item.setFlags(Qt.ItemIsEnabled)  # only read
                item.setText(str(step.get_maneuver().tolerance_xy))
                self.tableWidget.setItem(i, 10, item)

            elif step.get_maneuver().get_maneuver_type() == PARK_MANEUVER:

                if step.get_maneuver().use_yaw:

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText("Holonomic")
                    self.tableWidget.setItem(i, 6, item)

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText(str(step.get_maneuver().time))
                    self.tableWidget.setItem(i, 7, item)


                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText(str(math.degrees(step.get_maneuver().final_yaw)))
                    self.tableWidget.setItem(i, 8, item)

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText("")
                    self.tableWidget.setItem(i, 10, item)


                else:
                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText("Anchor")
                    self.tableWidget.setItem(i, 6, item)

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText(str(step.get_maneuver().time))
                    self.tableWidget.setItem(i, 7, item)

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText("")
                    self.tableWidget.setItem(i, 8, item)

                    item = QTableWidgetItem()
                    item.setFlags(Qt.ItemIsEnabled)  # only read
                    item.setText("")
                    self.tableWidget.setItem(i, 10, item)

            item = QTableWidgetItem()
            item.setFlags(Qt.ItemIsEnabled) # only read
            item.setText(str(step.get_maneuver().surge_velocity))
            self.tableWidget.setItem(i, 9, item)

            item = QTableWidgetItem()
            item.setFlags(Qt.ItemIsEnabled)  # only read
            actions = step.get_actions()
            str_actions = ""
            for act in actions:
                str_actions += act.get_action_id() + "\n"
            item.setText(str_actions)
            self.tableWidget.setItem(i, 11, item)
            # text_edit = QTextEdit()
            # # text_edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            #
            # text_edit.setReadOnly(True)
            # text_edit.setText(str_actions)
            # self.tableWidget.setCellWidget(i, 11, text_edit)

            for i in range(self.tableWidget.columnCount()):
                self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

            for i in range(self.tableWidget.rowCount()):
                self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)


    def resizeEvent(self, event):
        super(MissionTableDialog, self).resizeEvent(event)
