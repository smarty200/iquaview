"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Map tools for drawing Polygons on the canvas
"""

from math import radians, sqrt, degrees, atan2, pi
from qgis.gui import QgsMapTool, QgsRubberBand
from qgis.core import (QgsCoordinateTransform,
                       QgsGeometry,
                       QgsPointXY,
                       QgsDistanceArea,
                       QgsPoint,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsEllipse)

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QCursor, QPixmap, QColor

from iquaview_lib.utils import calcutils as calc
from iquaview.src.mission.maptools.rectangletools import Rectangle


# Works as an Abstract Class for the rest of the tools
class AbstractPolygonTool(QgsMapTool):
    message_signal = pyqtSignal(str)
    geometry_finished_signal = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas, segments=20, polygon_shape="ellipse"):
        """
        Init of the object PolygonToolBase
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param segments: Defines the number of segments that the polygon will have
        :type segments: int
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.polygon_segments = segments
        self.polygon_shape = polygon_shape
        self.length = 0
        self.n_points = 0
        self.polygon_rb = None
        self.rectangle_rb = None
        self.geometry_created = False
        self.p1, self.p2, self.p3 = None, None, None
        self.crs = QgsCoordinateReferenceSystem("EPSG:4326")
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(self.crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(self.crs.ellipsoidAcronym())
        self.rectangle = Rectangle(self.crs)

        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #1210f3",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.     .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.     .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.p1, self.p2, self.p3 = None, None, None
        self.geometry_created = False
        if self.rectangle_rb:
            self.rectangle_rb.reset(True)
        if self.polygon_rb:
            self.polygon_rb.reset(True)

    def reset_rubber_bands(self):
        """ Resets rubber bands """
        if self.rectangle_rb:
            self.rectangle_rb.reset(True)

        if self.polygon_rb:
            self.polygon_rb.reset(True)

        color = QColor(255, 0, 0, 128)

        # Create new rectangle rubber band
        self.rectangle_rb = QgsRubberBand(self.canvas, True)
        self.rectangle_rb.setColor(color)
        self.rectangle_rb.setWidth(1)
        self.rectangle_rb.setLineStyle(Qt.DashLine)
        self.rectangle_rb.setFillColor(Qt.transparent)

        # Create new polygon rubber band
        self.polygon_rb = QgsRubberBand(self.canvas, True)
        self.polygon_rb.setColor(color)
        self.polygon_rb.setWidth(1)

        self.rb_reset_signal.emit()

    def hide_bands(self):
        """ Hides rubber bands """
        if self.rectangle_rb:
            self.rectangle_rb.hide()

        if self.polygon_rb:
            self.polygon_rb.hide()

    def set_segments(self, segments):
        """
        Sets the number of segments or vertices that will have the polygon
        :param segments: number of segments
        :type segments: int
        """
        self.polygon_segments = segments

        if self.geometry_created:
            self.create_polygon_and_emit()

    def update_rubber_bands_from_geometry(self, rect_geom):
        """
        Updates the rectangle and polygon rubber band with the reometry of the rectangle porvided
        :param rect_geom: Geometry of the rectangle containing the figures in WGS 84 coordinates
        :rtype: QgsGeometry
        """
        trans_wgs_to_map = QgsCoordinateTransform(self.crs, self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        # Transform rect coordinates to map coordinates in order to set the rubber band correctly
        map_rect_points = list()
        for p in rect_geom.asPolygon()[0]:
            map_rect_points.append(trans_wgs_to_map.transform(p))
        map_rect_geom = QgsGeometry.fromPolygonXY([map_rect_points])
        self.rectangle_rb.setToGeometry(map_rect_geom)

        # generate the polygon geometry inside the rect geometry
        wgs_poly_geom = self.create_polygon(rect_geom.asPolygon()[0], self.polygon_shape)

        # Transform polygon coordinates to map coordinates in order to set the rubber band correctly
        map_poly_points = list()
        for p in wgs_poly_geom.asPolygon()[0]:
            map_poly_points.append(trans_wgs_to_map.transform(p))
        map_poly_geom = QgsGeometry.fromPolygonXY([map_poly_points])
        self.polygon_rb.setToGeometry(map_poly_geom)

    def create_polygon_and_emit(self):
        """
        Creates a polygon geometry from the rectangle geometry set with the tool and emits it.
        It also updates the rubber bands
        """
        if self.rectangle_rb is None or self.rectangle_rb.asGeometry().isNull():
            return

        rect_points = list()
        # Get geometries from rectangle and polygon
        rect_geom = self.rectangle_rb.asGeometry()
        geometries = rect_geom.asGeometryCollection()

        for geom in geometries:
            rect_points += geom.asPolygon()[0]

        # Rubber band geometry points are in map crs coordinates,
        # we must transform them to the 'standard' crs other methods use

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())
        wgs_rect_points = list()
        for p in rect_points:
            wgs_rect_points.append(trans_map_to_wgs.transform(p))

        poly_geom = self.create_polygon(wgs_rect_points, self.polygon_shape)
        wgs_rect_geom = QgsGeometry.fromPolygonXY([wgs_rect_points])

        # Emit the geometry
        self.geometry_finished_signal.emit(poly_geom)

        # Update rubber bands
        self.update_rubber_bands_from_geometry(wgs_rect_geom)

    def create_polygon(self, rect_points, polygon_shape="ellipse"):
        """
        Creates an polygon within rectangle with shape fixed by polygon_shape
        :param rect_points: list op points of the rectangle
        :type rect_points: list
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        :return: Polygon
        :rtype: QgsGeometry
        """
        if polygon_shape == "ellipse":
            poly_geom = self.polygon_from_ellipse_within_rectangle(rect_points)
        elif polygon_shape == "star":
            poly_geom = self.polygon_from_star_within_rectangle(rect_points)

        return poly_geom


    def polygon_from_ellipse_within_rectangle(self, rect_points):
        """
        Creates an ellipse from 4 rectangle points and approximates a polygon to the number of segments
        set in the object's variable
        :param rect_points: list of points of the rectangle
        :type rect_points: list
        :return: Polygon from the ellipse
        :rtype: QgsGeometry
        """

        geom = QgsGeometry.fromPolygonXY([rect_points])

        if self.polygon_segments > 4:  # Special cases are needed for 4 segments and 3 segments
            # Find ellipse properties
            semi_axis_1 = calc.distance_plane(rect_points[0].x(), rect_points[0].y(),
                                              rect_points[1].x(), rect_points[1].y()) / 2
            semi_axis_2 = calc.distance_plane(rect_points[1].x(), rect_points[1].y(),
                                              rect_points[2].x(), rect_points[2].y()) / 2
            center_point = geom.centroid().vertexAt(0)

            # Azimuth will be different depending on what axis is the major one
            if max(semi_axis_1, semi_axis_2) == semi_axis_1:
                azimuth = calc.get_angle_of_line_between_two_points(rect_points[1].x(), rect_points[1].y(),
                                                                    rect_points[0].x(), rect_points[0].y())
            else:
                azimuth = calc.get_angle_of_line_between_two_points(rect_points[1].x(), rect_points[1].y(),
                                                                    rect_points[2].x(), rect_points[2].y())

            # Correcting azimuth to start from the north to the first quadrant (north = 0, east = 90)
            azimuth = (azimuth - 90) * (-1)
            if azimuth > 180:
                azimuth = azimuth - 180

            ellipse = QgsEllipse(center_point, max(semi_axis_1, semi_axis_2), min(semi_axis_1, semi_axis_2), azimuth)

            # Transform ellipse points to a list of QgsPointsXY
            polygon_xy_points = list()
            for p in ellipse.toLineString(segments=self.polygon_segments):
                polygon_xy_points.append(QgsPointXY(p))

            poly_geom = QgsGeometry.fromPolygonXY([polygon_xy_points])

        elif self.polygon_segments == 4:
            poly_geom = geom

        elif self.polygon_segments == 3:
            p1 = rect_points[0]
            p2 = rect_points[1]
            p3_x, p3_y = calc.calc_middle_point(rect_points[2].x(), rect_points[2].y(),
                                                rect_points[3].x(), rect_points[3].y())
            p3 = QgsPointXY(p3_x, p3_y)
            poly_geom = QgsGeometry.fromPolygonXY([[p1, p2, p3]])

        else:
            return None

        return poly_geom

    def polygon_from_star_within_rectangle(self, rect_points):
        """
        Creates an star from 4 rectangle points and approximates a polygon to the number of segments
        set in the object's variable
        :param rect_points: list of points of the rectangle
        :type rect_points: list
        :return: Polygon from the ellipse
        :rtype: QgsGeometry
        """

        geom = QgsGeometry.fromPolygonXY([rect_points])

        if self.polygon_segments >= 3:  # Special cases are needed for 4 segments and 3 segments

            # Find ellipse properties
            center_point = geom.centroid().vertexAt(0)
            mid_point_one = calc.calc_middle_point(rect_points[0].x(), rect_points[0].y(), rect_points[1].x(), rect_points[1].y())
            mid_point_two = calc.calc_middle_point(rect_points[1].x(), rect_points[1].y(), rect_points[2].x(), rect_points[2].y())

            angle_one = calc.get_angle_of_line_between_two_points(rect_points[0].x(), rect_points[0].y(), rect_points[1].x(), rect_points[1].y(), "radians")
            angle_two = calc.get_angle_of_line_between_two_points(rect_points[1].x(), rect_points[1].y(), rect_points[2].x(), rect_points[2].y(), "radians")

            dist_one = calc.distance_plane(mid_point_one[0], mid_point_one[1], center_point.x(), center_point.y())
            dist_two = calc.distance_plane(mid_point_two[0], mid_point_two[1], center_point.x(), center_point.y())

            if min(dist_one, dist_two) == dist_one:
                mid_point = mid_point_one
                angle = 0.0 - angle_one
            else:
                mid_point = mid_point_two
                angle = 0.0 - angle_two

            new_area = list()
            radius = calc.distance_ellipsoid(center_point.x(), center_point.y(),
                                             mid_point[0], mid_point[1])
            # Compute coverage points
            angles = []
            for i in range(self.polygon_segments*2):
                angles.append(angle)
                angle += pi / float(self.polygon_segments)

            for angle in angles:
                lon_1, lat_1 = calc.endpoint_sphere(center_point.x(), center_point.y(), radius, degrees(angle))
                p1 = QgsPointXY(lon_1, lat_1)
                new_area.append(p1)

            poly_geom = QgsGeometry.fromPolygonXY([new_area])

        else:
            return None

        return poly_geom


class PolygonBy3PointsTool(AbstractPolygonTool):

    def __init__(self, canvas, segments=20, polygon_shape="ellipse"):
        """
        Init of the object PolygonBy3PointsTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param segments: Defines the number of segments that the polygon will have
        :type segments: int
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        """
        AbstractPolygonTool.__init__(self, canvas, segments, polygon_shape)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.p1, self.p2, self.p3 = None, None, None

            self.reset_rubber_bands()

            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already drawn.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  self.crs,
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.geometry_created:  # Reset. A click was made to create a new polygon
            self.p1, self.p2, self.p3 = None, None, None
            self.geometry_created = False
            self.n_points = 0

        # Define first point
        if self.n_points == 0:
            self.reset_rubber_bands()
            self.p1 = point
            self.n_points += 1
            self.message_signal.emit("Define bearing and extent along track")

        # Define second point
        elif self.n_points == 1 and (self.p1.x() != point.x() or self.p1.y() != point.y()):
            # First and second points must be different to generate the geometry correctly
            self.p2 = point
            self.n_points += 1
            self.message_signal.emit("Define extent across track")

        # Define third and last point
        elif self.n_points == 2 and (self.p2.x() != point.x() or self.p2.y() != point.y()):
            # Second and third points also must be different
            self.p3 = point

            self.create_polygon_and_emit()
            self.geometry_created = True

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rectangle_rb or self.n_points == 0:
            return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(self.crs, self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.n_points == 1:
            # Create a line geometry for the rectangle rubber band
            p1 = QgsPoint(trans_wgs_to_map.transform(self.p1))
            p2 = QgsPoint(map_pos)
            geom = QgsGeometry.fromPolyline([p1, p2])
            self.rectangle_rb.setToGeometry(geom, None)

            # Update message bar data
            curr_dist = self.distance.measureLine(self.p1, currpoint)
            curr_bearing = degrees(self.distance.bearing(self.p1, currpoint))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.message_signal.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))

        if self.n_points >= 2 and not self.geometry_created:
            # Create a rectangle geometry for the rectangle rubber band
            geom = self.rectangle.get_rect_by3_points(self.p1, self.p2, currpoint)

            self.update_rubber_bands_from_geometry(geom)

            # Update message bar data
            p3 = QgsPointXY(geom.vertexAt(2))
            curr_dist = self.distance.measureLine(self.p1, currpoint)
            curr_bearing = degrees(self.distance.bearing(self.p2, p3))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing

            self.message_signal.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))


class PolygonByFixedExtentTool(AbstractPolygonTool):

    def __init__(self, canvas, x_length=0.0, y_length=0.0, segments=20, polygon_shape="ellipse"):
        """
        Init of the object PolygonByFixedExtentTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param segments: Defines the number of segments that the polygon will have
        :type segments: int
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        """
        AbstractPolygonTool.__init__(self, canvas, segments, polygon_shape)

        self.fixed_p2, self.fixed_p3 = QgsPointXY(0, 0), QgsPointXY(0, 0)
        self.x_length = x_length
        self.y_length = y_length
        self.bearing = 0.0
        self.last_point_clicked = None

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.p1, self.p2, self.p3 = None, None, None

            self.reset_rubber_bands()
            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(point_map.x(), point_map.y())

        if self.geometry_created:  # Reset. A click was made to create a new polygon
            self.p1, self.p2, self.p3 = None, None, None
            self.geometry_created = False
            self.n_points = 0
            self.last_point_clicked = None

        # Define first point
        if self.n_points == 0:
            self.reset_rubber_bands()

            self.message_signal.emit("Define bearing along track")

            self.p1 = point
            self.last_point_clicked = point
            self.n_points += 1

        # Define second point
        elif self.n_points == 1 and self.last_point_clicked != point:
            # First and second points must be different to generate the geometry correctly
            self.bearing = self.distance.bearing(self.p1, self.p2)
            self.n_points += 1
            self.last_point_clicked = point
            self.message_signal.emit("Define across track direction")

        # Define third and last point
        elif self.n_points == 2 and self.last_point_clicked != point:
            # Second and third points also must be different
            self.create_polygon_and_emit()
            self.geometry_created = True
            self.message_signal.emit("")

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rectangle_rb or self.geometry_created or self.n_points == 0:
            return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(self.crs, self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(point_map.x(), point_map.y())

        if self.n_points == 1:
            self.bearing = self.distance.bearing(self.p1, currpoint)
            self.p2 = self.distance.measureLineProjected(self.p1, self.x_length, self.bearing)[1]

            # Create a line geometry for the rectangle rubber band
            p1 = QgsPoint(trans_wgs_to_map.transform(self.p1))
            p2 = QgsPoint(trans_wgs_to_map.transform(self.p2))
            geom = QgsGeometry.fromPolyline([p1, p2])
            self.rectangle_rb.setToGeometry(geom, None)

            # Update message bar data
            curr_bearing = degrees(self.bearing)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.message_signal.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.x_length, curr_bearing))

        if self.n_points >= 2:
            # test if currpoint is left or right of the line defined by p1 and p2
            side = calc.calc_is_collinear(self.p1.x(), self.p1.y(),
                                          self.p2.x(), self.p2.y(),
                                          currpoint.x(), currpoint.y())

            if side == 0:
                return None

            # Update geometry and rubber bands
            self.p3 = self.distance.measureLineProjected(self.p2, self.y_length, self.bearing + radians(90) * side)[1]
            geom = self.rectangle.get_rect_by3_points(self.p1, self.p2, self.p3, self.y_length)
            self.update_rubber_bands_from_geometry(geom)

            # Update message bar data
            curr_bearing = degrees(self.bearing + radians(90) * side)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.message_signal.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.y_length, curr_bearing))

    def set_x_length(self, length):
        """
        Sets the length of the polygon in the x axis and updates the geometry
        :param length: length of the polygon
        :type length: float
        """

        self.x_length = length

        if self.geometry_created:
            self.update_lengths()

    def set_y_length(self, length):
        """
        Sets the length of the polygon in the y axis and updates the geometry
        :param length: length of the polygon
        :type length: float
        """
        self.y_length = length

        if self.geometry_created:
            self.update_lengths()

    def update_lengths(self):
        """ Recalculates the geometry for the rubber bands """
        side = calc.calc_is_collinear(self.p1.x(), self.p1.y(),
                                      self.p2.x(), self.p2.y(),
                                      self.p3.x(), self.p3.y())
        if side == 0 or self.y_length == 0 or self.x_length == 0:
            return None

        self.p2 = self.distance.measureLineProjected(self.p1, self.x_length, self.bearing)[1]
        self.p3 = self.distance.measureLineProjected(self.p2, self.y_length, self.bearing + radians(90) * side)[1]

        geom = self.rectangle.get_rect_by3_points(self.p1, self.p2, self.p3, self.y_length)

        self.update_rubber_bands_from_geometry(geom)
        self.create_polygon_and_emit()


class PolygonFromCenterTool(AbstractPolygonTool):

    def __init__(self, canvas, segments=20, polygon_shape="ellipse"):
        """
        Init of the object PolygonFromCenterTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param segments: Defines the number of segments that the polygon will have
        :type segments: int
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        """
        AbstractPolygonTool.__init__(self, canvas, segments, polygon_shape)

        self.shift_pressed = False
        self.rect_ini_geom = None
        self.rect_curr_geom = None
        self.last_currpoint = None
        self.center_point = None

        self.x_length = 0
        self.y_length = 0
        self.curr_angle = 0.0
        self.total_angle = 0.0
        self.angle_ini_rot = 0.0

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the shift key is pressed, sets
        shift_pressed to True and creates a point and angle of rotation.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = True

            trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                      QgsProject.instance().transformContext())

            map_pos = self.toMapCoordinates(self.canvas.mouseLastXY())
            point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())
            self.point_ini_rot = point
            self.angle_ini_rot = self.curr_angle

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is released,
        set shift_pressed to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = False

        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.reset_rubber_bands()
            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if no rectangle was started or finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.geometry_created:  # Reset. A click was made to create a new polygon
            self.geometry_created = False
            self.n_points = 0

        if self.n_points == 0:
            self.reset_rubber_bands()
            self.center_point = point
            self.n_points += 1

        elif self.n_points == 1 and self.center_point != point:
            self.geometry_created = True

            self.create_polygon_and_emit()
            self.rect_curr_geom = None

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rectangle_rb or self.geometry_created or self.n_points == 0:
            return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if not self.shift_pressed:
            self.rect_curr_geom = self.rectangle.get_rect_from_center(self.center_point, currpoint, self.curr_angle)
            self.rect_ini_geom = self.rect_curr_geom

            if self.curr_angle != 0:
                self.rect_curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.rect_curr_geom,
                                                                                       self.center_point,
                                                                                       delta=self.curr_angle)

            self.x_length = self.distance.measureLine(self.rect_curr_geom.asPolygon()[0][0],
                                                      self.rect_curr_geom.asPolygon()[0][1])
            self.y_length = self.distance.measureLine(self.rect_curr_geom.asPolygon()[0][1],
                                                      self.rect_curr_geom.asPolygon()[0][2])

            self.rect_curr_geom = self.rectangle.get_rect_projection(self.rect_curr_geom, self.center_point,
                                                                     self.x_length, self.y_length)

        elif self.rect_ini_geom is not None:
            if self.last_currpoint is None:
                self.last_currpoint = currpoint

            self.rect_curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.rect_ini_geom,
                                                                                   self.center_point,
                                                                                   currpoint, self.point_ini_rot,
                                                                                   self.angle_ini_rot)

            self.last_currpoint = currpoint
            self.rect_curr_geom = self.rectangle.get_rect_projection(self.rect_curr_geom, self.center_point,
                                                                     self.x_length, self.y_length)

        if self.rect_curr_geom is not None:
            self.update_rubber_bands_from_geometry(self.rect_curr_geom)

        self.message_signal.emit(
            "Adjust lengths along track and across track. Hold Shift to adjust track orientation. Click when finished.")


class PolygonFromCenterFixedTool(AbstractPolygonTool):

    def __init__(self, canvas, x_length=0.0, y_length=0.0, segments=20, polygon_shape="ellipse"):
        """
        Init of the object PolygonFromCenterFixedTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param segments: Defines the number of segments that the polygon will have
        :type segments: int
        :param polygon_shape: shape of the polygon
        :type polygon_shape: str
        """
        AbstractPolygonTool.__init__(self, canvas, segments, polygon_shape)

        self.shift_pressed = False
        self.rect_ini_geom = None
        self.rect_curr_geom = None
        self.last_currpoint = None
        self.center_point = None
        self.p2 = None

        self.x_length = x_length
        self.y_length = y_length
        self.diagonal = 0.0
        self.curr_angle = 0.0
        self.total_angle = 0.0
        self.angle_ini_rot = 0.0

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is pressed, sets
        shift_pressed to True and creates apoint and angle of rotation.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = True

            trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                      QgsProject.instance().transformContext())

            map_pos = self.toMapCoordinates(self.canvas.mouseLastXY())
            point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())
            self.point_ini_rot = point
            self.angle_ini_rot = self.curr_angle

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is released,
        set shift_pressed to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = False

        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.curr_angle = 0.0
            self.total_angle = 0.0
            self.last_currpoint = None
            self.center_point = None
            self.p2 = None
            self.reset_rubber_bands()
            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.geometry_created:  # Reset. A click was made to create a new polygon
            self.geometry_created = False
            self.last_currpoint = None
            self.rect_curr_geom = None
            self.n_points = 0
            self.curr_angle = 0.0
            self.total_angle = 0.0

        if self.n_points == 0 and self.x_length != 0.0 and self.y_length != 0.0:
            self.reset_rubber_bands()
            self.center_point = point

            if self.x_length != 0:
                self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)
                self.p2 = self.distance.measureLineProjected(self.center_point, self.diagonal,
                                                             atan2(self.y_length / 2, self.x_length / 2))[1]
            self.n_points += 1

        elif self.n_points == 1 and self.center_point != point:
            self.geometry_created = True
            self.shift_pressed = False
            self.create_polygon_and_emit()

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rectangle_rb or self.geometry_created or self.x_length == 0.0 or self.y_length == 0.0 \
                or self.n_points == 0:
            return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(), self.crs,
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if not self.shift_pressed:
            if self.last_currpoint is None:
                self.last_currpoint = self.p2
                self.rect_ini_geom = self.rectangle.get_rect_from_center(self.center_point, self.last_currpoint)

                self.rect_curr_geom = self.rectangle.get_rect_projection(self.rect_ini_geom, self.center_point,
                                                                         self.x_length, self.y_length)

        elif self.rect_ini_geom is not None:
            if self.last_currpoint is None:
                self.last_currpoint = currpoint

            self.rect_curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.rect_ini_geom,
                                                                                   self.center_point,
                                                                                   currpoint, self.point_ini_rot,
                                                                                   self.angle_ini_rot)

            self.last_currpoint = currpoint
            self.rect_curr_geom = self.rectangle.get_rect_projection(self.rect_curr_geom, self.center_point,
                                                                     self.x_length, self.y_length)

        if self.rect_curr_geom is not None:
            self.update_rubber_bands_from_geometry(self.rect_curr_geom)

        self.message_signal.emit("Hold Shift to adjust track orientation. Click when finished.")

    def set_x_length(self, length):
        """
        Sets the length of the polygon in the x axis and updates the geometry
        :param length: length of the polygon
        :type length: float
        """

        self.x_length = length

        if self.geometry_created:
            self.update_lengths()

    def set_y_length(self, length):
        """
        Sets the length of the polygon in the y axis and updates the geometry
        :param length: length of the polygon
        :type length: float
        """

        self.y_length = length

        if self.geometry_created:
            self.update_lengths()

    def update_lengths(self):
        """ Recalculates the geometry for the rubber bands """
        if self.y_length == 0 or self.x_length == 0:
            return

        self.rect_curr_geom = self.rectangle.get_rect_projection(self.rect_curr_geom, self.center_point,
                                                                 self.x_length, self.y_length)
        self.update_rubber_bands_from_geometry(self.rect_curr_geom)
        self.create_polygon_and_emit()
