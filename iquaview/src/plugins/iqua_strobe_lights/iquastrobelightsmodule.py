"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Class to add the Iqua Strobe Lights functions to the mainwindow
"""

from importlib import util

from PyQt5.QtWidgets import QAction, QToolBar
from PyQt5.QtCore import Qt, QObject

if util.find_spec("iquaview_iqua_strobe_lights") is not None:
    import iquaview_iqua_strobe_lights


class IquaStrobeLightsModule(QObject):

    def __init__(self, vehicle_info, menubar, view_menu_toolbar, parent=None):
        super(IquaStrobeLightsModule, self).__init__()

        self.menubar = menubar
        self.view_menu_toolbar = view_menu_toolbar
        self.vehicle_info = vehicle_info

        self.iqua_strobe_lights_widget = iquaview_iqua_strobe_lights.IquaStrobeLightsWidget(self.vehicle_info)
        if util.find_spec('iquaview_flir_spinnaker_camera') is None:

            self.iqua_strobe_lights_action = QAction(
                "Iqua Strobe Lights",
                self)

            self.menubar.addActions([self.iqua_strobe_lights_action])

            # Toolbar for Iqua Strobe Lights
            self.iqua_strobe_lights_toolbar = QToolBar("Iqua Strobe Lights")
            self.iqua_strobe_lights_toolbar.setObjectName("Iqua Strobe Lights")
            self.iqua_strobe_lights_toolbar.addWidget(self.iqua_strobe_lights_widget)
            parent.addToolBar(self.iqua_strobe_lights_toolbar)

            self.iqua_strobe_lights_view_action = QAction(
                "Iqua Strobe Lights",
                self)
            self.iqua_strobe_lights_view_action.setCheckable(True)
            self.iqua_strobe_lights_view_action.setChecked(True)
            self.iqua_strobe_lights_view_action.triggered.connect(lambda: self.change_toolbar_visibility(self.iqua_strobe_lights_toolbar))

            view_menu_toolbar.addAction(self.iqua_strobe_lights_view_action)

        self.iqua_strobe_lights_widget.connect()


    def change_toolbar_visibility(self, toolbar):
        """
        Change toolbar visibility

        :param toolbar: toolbar
        """
        sender = self.sender()
        if sender.isChecked():
            toolbar.setVisible(True)
        else:
            toolbar.setVisible(False)

    @staticmethod
    def version():
        return "{} {}".format(iquaview_iqua_strobe_lights.PLUGIN_NAME, iquaview_iqua_strobe_lights.__version__)

    def disconnect_module(self):
        """ Close dialog"""
        self.iqua_strobe_lights_widget.close()

    def remove(self):
        """ Removes module from iquaview"""
        self.disconnect_module()
        self.menubar.removeAction(self.iqua_strobe_lights_action)
