"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to connect the USBL device and display information from it.
 It displays the USBL track, GPS track and the track according to the AUV navigation
 that is transmitted back acoustically in the map canvas.
 Also shows some status information retrieved from the received acoustic communication.
"""

import math
import time
import logging
from importlib import util

from PyQt5.QtWidgets import QWidget, QMessageBox
from PyQt5.QtCore import QTimer, Qt, pyqtSignal
from PyQt5.QtGui import QColor

from qgis.core import QgsPointXY, QgsWkbTypes, QgsCoordinateTransform, QgsCoordinateReferenceSystem, QgsProject

from iquaview.src.ui.ui_usbl import Ui_USBLWidget
from iquaview.src.canvastracks.canvasmarker import CanvasMarker
from iquaview_lib.cola2api.gps_driver import GPSDriver, GPSPosition, GPSOrientation, ConfigGPSDriver
from iquaview_lib.cola2api.basic_message import BasicMessage
from iquaview_lib.utils import calcutils

if util.find_spec('iquaview_evologics_usbl') is not None:
    import iquaview_evologics_usbl

logger = logging.getLogger(__name__)


def version():
    return "iquaview_evologics_usbl " + iquaview_evologics_usbl.__version__


class USBLWidget(QWidget, Ui_USBLWidget):
    usbl_connected = pyqtSignal(bool)
    gpsconnectionfailed = pyqtSignal()
    mission_started = pyqtSignal()
    mission_stopped = pyqtSignal()
    gps_position_orientation_signal = pyqtSignal(GPSPosition, GPSOrientation)
    usbl_data_signal = pyqtSignal(BasicMessage)
    auv_data_signal = pyqtSignal(BasicMessage)

    def __init__(self, canvas, config, vehicle_info, mission_sts, parent=None):
        super(USBLWidget, self).__init__(parent)
        self.setupUi(self)

        self.canvas = canvas
        self.config = config
        self.vehicle_info = vehicle_info
        self.mission_sts = mission_sts
        self.data = None
        self.data_auv = None
        self.data_gps_position = None
        self.data_gps_orientation = None

        self.default_color_gps = QColor(Qt.darkGreen)
        vessel_name = self.config.csettings['vessel_name']
        width = self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"]
        length = self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"]
        self.marker_gps = CanvasMarker(self.canvas, self.default_color_gps, ":/resources/vessel.svg", width, length,
                                       marker_mode=True, config=config)

        self.trackwidget_gps.init("GPS track",
                                  self.canvas,
                                  self.default_color_gps,
                                  QgsWkbTypes.LineGeometry,
                                  self.marker_gps,
                                  has_connection_lost_marker=True)

        self.default_color_usbl = Qt.red
        self.marker_usbl = CanvasMarker(self.canvas, self.default_color_usbl,
                                        None, orientation=False)
        self.trackwidget_usbl.init("USBL track",
                                   self.canvas,
                                   self.default_color_usbl,
                                   QgsWkbTypes.PointGeometry,
                                   self.marker_usbl,
                                   has_connection_lost_marker=True)

        self.default_color_auv = QColor(Qt.darkYellow)
        self.default_color_auv.setAlpha(80)
        #":/resources/" + vehicle_info.get_vehicle_type() + "/vehicle.svg"
        self.marker_auv = CanvasMarker(self.canvas, self.default_color_auv,
                                       None,
                                       float(vehicle_info.get_vehicle_width()),
                                       float(vehicle_info.get_vehicle_length()))

        self.trackwidget_auv.init("AUV track",
                                  self.canvas,
                                  self.default_color_auv,
                                  QgsWkbTypes.LineGeometry,
                                  self.marker_auv,
                                  has_connection_lost_marker=True)
        # set signals
        self.connectButton.clicked.connect(self.connect)

        self.connected = False
        self.gps_connection_failed = False
        self.set_label_disconnected()
        self.timer = QTimer()
        self.timer.timeout.connect(self.usbl_update_canvas)
        self.gpsconnectionfailed.connect(self.stop_canvas_updates)

        self.last_ping = 0
        self.last_usbl_time = 0.0
        self.last_auv_time = 0.0

        self.controller = iquaview_evologics_usbl.EvologicsUSBLController(config)

    def connect(self):
        if not self.controller.is_connected():
            try:
                self.controller.connect()

                self.connectButton.setText("Disconnect")
                self.timer.start(1000)
                self.usbl_status_label.setText("Connected")
                self.usbl_status_label.setStyleSheet('font:italic; color:green')
                self.usbl_connected.emit(True)

                if self.trackwidget_auv.connection_lost_marker.isVisible():
                    self.trackwidget_auv.connection_lost_marker.hide()
                if self.trackwidget_gps.connection_lost_marker.isVisible():
                    self.trackwidget_gps.connection_lost_marker.hide()
                if self.trackwidget_usbl.connection_lost_marker.isVisible():
                    self.trackwidget_usbl.connection_lost_marker.hide()

                self.gps_connection_failed = False
                if not self.controller.e_usbl.get_gps_initialized():
                    self.stop_canvas_updates()

            except Exception as e:
                logger.error("No connection to USBL: {}".format(e))
                QMessageBox.critical(self, "USBL Connection Failed",
                                     "Connection with USBL could not be established: \n"+str(e),
                                     QMessageBox.Close)
                self.set_label_disconnected()
                # self.connected = False
                self.disconnect()
        else:
            self.disconnect()

    def usbl_update_canvas(self):
        if self.controller.is_connected():
            pos_auv = None
            pos_usbl = None
            auv_heading = None
            data_gps_position: GPSPosition
            data_gps_orientation: GPSOrientation

            # get data
            data_usbl = self.controller.usbl_update_sensed_on_surface_data()
            data_auv = self.controller.usbl_update_auv_data()
            data_gps_position, data_gps_orientation = self.controller.usbl_update_gps_data()
            self.gps_position_orientation_signal.emit(data_gps_position, data_gps_orientation)
            self.usbl_data_signal.emit(data_usbl)
            self.auv_data_signal.emit(data_auv)

            # save values
            self.data = data_usbl
            self.data_auv = data_auv
            self.data_gps_position = data_gps_position
            self.data_gps_orientation = data_gps_orientation

            # if valid USBL data
            if data_usbl.time != 0.0 and data_usbl.time != self.last_usbl_time:
                logger.info("Received data measured by USBL")
                # show position in correct reference system
                usbl_crs = QgsCoordinateReferenceSystem("EPSG:4326")
                if usbl_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
                    trans = QgsCoordinateTransform(usbl_crs,
                                                   self.canvas.mapSettings().destinationCrs(),
                                                   QgsProject.instance().transformContext())
                    pos_usbl = trans.transform(data_usbl.longitude, data_usbl.latitude)
                else:
                    pos_usbl = QgsPointXY(data_usbl.longitude, data_usbl.latitude)
                # show position as text
                usbl_position = "{:.5F}, {:.5F}".format(data_usbl.latitude, data_usbl.longitude)
                self.usbl_position_label.setText(usbl_position)
                self.usbl_depth_label.setText("{:.3F}".format(-data_usbl.depth))
                # update last ping
                self.last_ping = 0
                # save time to ignore same time messages
                self.last_usbl_time = data_usbl.time
            else:
                # increment last ping
                self.last_ping = self.last_ping + 1
            # show last ping as text
            self.usbl_lastping_label.setText(str(self.last_ping))

            # if valid AUV data
            if data_auv.time != 0.0 and data_auv.time != self.last_auv_time:
                logger.info("Received data back from AUV")
                # save time to ignore same time messages
                self.last_auv_time = data_auv.time
                # update heading
                auv_heading = data_auv.heading_accuracy
                # show position in correct reference system
                auv_crs = QgsCoordinateReferenceSystem("EPSG:4326")
                if auv_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
                    trans = QgsCoordinateTransform(auv_crs,
                                                   self.canvas.mapSettings().destinationCrs(),
                                                   QgsProject.instance().transformContext())
                    pos_auv = trans.transform(data_auv.longitude, data_auv.latitude)
                else:
                    pos_auv = QgsPointXY(data_auv.longitude, data_auv.latitude)
                # show position as text
                auv_position = "{:.5F}, {:.5F}".format(data_auv.latitude, data_auv.longitude)
                self.auv_position_label.setText(auv_position)
                self.auv_depth_label.setText("{:.2F} m / {:.2F} m".format(data_auv.depth, data_auv.altitude))
                # check status_code
                self.mission_sts.assign_status(data_auv.command_error)
                [status, status_color] = self.mission_sts.get_status()
                if status == "":
                    status = "connected"
                    status_color = 'green'
                self.usbl_status_label.setText(status)
                self.usbl_status_label.setStyleSheet('font:italic; color:{}'.format(status_color))

                if pos_auv is not None and auv_heading is not None:
                    self.trackwidget_auv.track_update_canvas(pos_auv, auv_heading)

            if data_gps_position.is_new():

                if (data_gps_position.quality >= 1) and (data_gps_position.quality <= 5):
                    gps_lat = data_gps_position.latitude
                    gps_lon = data_gps_position.longitude
                    gps_heading = data_gps_orientation.orientation_deg

                    vessel_name = self.config.csettings['vessel_name']
                    brng = (math.radians(gps_heading)
                            + math.atan2(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                             + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                                         self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                             + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x']))
                    distance = - math.sqrt(
                        math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                     + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x'],
                                 2)
                        + math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                       + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                                   2))  # Distance in m

                    gps_lon, gps_lat = calcutils.endpoint_sphere(gps_lon, gps_lat, distance,
                                                                 math.degrees(brng))

                    gps_crs = QgsCoordinateReferenceSystem("EPSG:4326")
                    if gps_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
                        trans = QgsCoordinateTransform(gps_crs,
                                                       self.canvas.mapSettings().destinationCrs(),
                                                       QgsProject.instance().transformContext())
                        pos_gps = trans.transform(gps_lon, gps_lat)
                    else:
                        pos_gps = QgsPointXY(gps_lon, gps_lat)

                    # update canvas
                    self.trackwidget_gps.track_update_canvas(pos_gps, math.radians(gps_heading))

                # update canvas
                if pos_usbl is not None:
                    self.trackwidget_usbl.track_update_canvas(pos_usbl, 0)

                # Show gps quality
                self.gps_fix_quality_label.setText(data_gps_position.get_fix_quality_as_string())

            elif not data_gps_position.is_new():
                now = time.time()
                if (now - data_gps_position.time) > 5.0:
                     self.gpsconnectionfailed.emit()

    def send_abort_and_surface(self):
        self.controller.send_abort_and_surface()
        self.usbl_status_label.setText("Sending Abort and Surface...")

    def send_emergency_surface(self):
        self.controller.send_emergency_surface()
        self.usbl_status_label.setText("Sending Emergency Surface...")

    def send_informative(self):
        logger.info("Setting USBL command to Informative")
        self.controller.send_informative()
        self.usbl_status_label.setText("Sending Informative Cmd...")

    def send_start_mission(self):
        logger.info("Start mission from  USBL...")
        self.usbl_status_label.setText("Sending Start Mission...")
        self.controller.send_start_mission()

    def send_stop_mission(self):
        logger.info("Stop mission from  USBL...")
        self.controller.send_stop_mission()
        self.usbl_status_label.setText("Sending Stop Mission...")

    def set_enable_update(self, enable):
        logger.info("Set {} updates from USBL...".format(enable))
        self.controller.set_enable_update(enable)

    def send_reset_timeout(self):
        logger.info("Send reset timeout from USBL...")
        self.controller.send_reset_timeout()
        self.usbl_status_label.setText("Sending Reset Timeout...")

    def stop_canvas_updates(self):
        self.trackwidget_usbl.close()
        self.trackwidget_gps.close()
        # self.trackwidget_auv.close()

        if not self.gps_connection_failed:
            QMessageBox.critical(self,
                                 "Gps connection failed",
                                 "Gps connection was lost. "
                                 "You have connection with the USBL but you can not show the position on the map",
                                 QMessageBox.Close)
            self.gps_connection_failed = True


    def clear_tracks(self):
        """ Clears the usbl, gps and auv tracks and markers """
        self.trackwidget_usbl.close()
        self.trackwidget_gps.close()
        self.trackwidget_auv.close()

    def set_label_disconnected(self):
        self.usbl_status_label.setText("Disconnected")
        self.usbl_status_label.setStyleSheet('font:italic; color:red')
        self.gps_fix_quality_label.setText("")

    def check_mission_start(self):
        while self.waiting_mission_start:
            logger.info("Waiting for mission to start...")
            mission_executing = self.mission_sts.is_mission_in_execution()
            if mission_executing:
                self.controller.send_informative()
                self.waiting_mission_start = False
                self.usbl_status_label.setText("Mission Started")
                self.mission_started.emit()
            time.sleep(1.0)

    def check_mission_stop(self):
        while self.waiting_mission_stop:
            mission_executing = self.mission_sts.is_mission_in_execution()
            if not mission_executing:
                self.controller.send_informative()
                self.waiting_mission_stop = False
                self.usbl_status_label.setText("Mission Stopped")
                self.mission_stopped.emit()
            time.sleep(1.0)

    def is_connected(self):
        return self.controller.is_connected()

    def update_width_and_length(self):
        vessel_name = self.config.csettings['vessel_name']
        width = self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"]
        length = self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"]
        self.marker_gps.set_width(width)
        self.marker_gps.set_length(length)
        self.usbl_update_canvas()

    def set_period(self):
        self.controller.usbl_set_period()

    def get_usbl_data(self):
        return self.data

    def get_auv_data(self):
        return self.data_auv

    def get_gps_data(self):
        """
        Return gps position and gps heading
        :return: return position and heading
        :rtype: [GPSPosition, GPSOrientation]
        """
        return self.data_gps_position, self.data_gps_orientation

    def disconnect(self):
        """ Sets the widget as disconnected """
        self.last_ping = 0
        self.last_usbl_time = 0.0
        self.last_auv_time = 0.0

        self.controller.disconnect()

        if self.timer:
            self.timer.stop()

        self.connectButton.setText("Connect")
        self.set_label_disconnected()
        self.usbl_connected.emit(False)

        if not self.trackwidget_auv.connection_lost_marker.isVisible():
            self.trackwidget_auv.connection_lost_marker.show()
        if not self.trackwidget_gps.connection_lost_marker.isVisible():
            self.trackwidget_gps.connection_lost_marker.show()
        if not self.trackwidget_usbl.connection_lost_marker.isVisible():
            self.trackwidget_usbl.connection_lost_marker.show()
