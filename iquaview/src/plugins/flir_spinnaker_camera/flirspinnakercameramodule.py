"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Class to add the Flir Spinnaker Camera functions to the mainwindow
"""

from importlib import util

from PyQt5.QtWidgets import QAction
from PyQt5.QtCore import QObject

if util.find_spec('iquaview_flir_spinnaker_camera') is not None:
    import iquaview_flir_spinnaker_camera


class FlirSpinnakerCameraModule(QObject):

    def __init__(self, vehicle_info, menubar, parent=None):
        super(QObject, self).__init__()

        self.vehicle_info = vehicle_info
        self.flir_spinnaker_camera_action = QAction(
            "Flir Spinnaker Camera",
            self)
        menubar.addActions([self.flir_spinnaker_camera_action])
        self.flir_spinnaker_camera_action.triggered.connect(self.open_flir_spinnaker_camera)

        self.flir_spinnaker_camera_dlg = iquaview_flir_spinnaker_camera.FlirSpinnakerCameraDlg(self.vehicle_info, parent.logger_value)

    def open_flir_spinnaker_camera(self):
        """ Open flir spinnaker camera dialog"""
        self.flir_spinnaker_camera_dlg.auv_config_params.load_section("Flir Spinnaker Camera")
        self.flir_spinnaker_camera_dlg.connect()
        self.flir_spinnaker_camera_dlg.showMaximized()

    def add_iqua_strobe_lights(self, iqua_strobe_lights_widget):
        """

        :param iqua_strobe_lights:
        :return:
        """
        self.flir_spinnaker_camera_dlg.toolbar.addSeparator()
        self.flir_spinnaker_camera_dlg.toolbar.addWidget(iqua_strobe_lights_widget)

    @staticmethod
    def version():
        return "iquaview_flir_spinnaker_camera " + iquaview_flir_spinnaker_camera.__version__

    def disconnect_module(self):
        """ Close dialog"""
        self.flir_spinnaker_camera_dlg.close()
