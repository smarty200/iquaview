# Copyright (c) 2021 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

"""
    Class to add the KVH GEOFOG3D INS functions to the mainwindow
"""
from importlib import util

from PyQt5.QtWidgets import QAction, QToolBar
from PyQt5.QtGui import QIcon

from iquaview_lib.utils.pluginbase import PluginBase
if util.find_spec('iquaview_kvh_geofog3d_ins') is not None:
    import iquaview_kvh_geofog3d_ins

MODULE_NAME = 'iquaview_kvh_geofog3d_ins'


class KVHModule(PluginBase):

    def __init__(self, menubar, view_menu_tooblar, vehicle_info, parent=None):
        super(KVHModule, self).__init__(MODULE_NAME, parent)
        self.module_found = True if (util.find_spec(MODULE_NAME) is not None) else False
        if self.is_module_found():
            self.menubar = menubar
            self.view_menu_toolbar = view_menu_tooblar
            parent = parent
            self.vehicle_info = vehicle_info

            self.kvh_geofog3d_ins = iquaview_kvh_geofog3d_ins.KVHGeofog3dINS(self.vehicle_info)

            # Actions for Vehicle (KVH)
            self.reset_sensor_action = QAction(
                QIcon(":/resources/kvh00.svg"),
                "KVH: Reset Sensor",
                self)
            self.set_imu_heading_action = QAction(
                QIcon(":/resources/kvh01.svg"),
                "KVH: Set IMU Heading",
                self)
            self.set_heading_deg_action = QAction(
                QIcon(":/resources/kvh02.svg"),
                "KVH: Set Heading deg",
                self)

            self.kvh_menu = self.menubar.addMenu("KVH")
            self.kvh_menu.addActions([self.reset_sensor_action,
                                      self.set_imu_heading_action,
                                      self.set_heading_deg_action])

            # Toolbar for Vehicle (kvh)
            self.kvh_toolbar = QToolBar("KVH Tools")
            self.kvh_toolbar.setObjectName("KVH Tools")
            self.kvh_toolbar.addAction(self.reset_sensor_action)
            self.kvh_toolbar.addAction(self.set_imu_heading_action)
            self.kvh_toolbar.addAction(self.set_heading_deg_action)

            self.kvh_toolbar_action = QAction("KVH", self)
            self.kvh_toolbar_action.setCheckable(True)
            self.kvh_toolbar_action.setChecked(True)
            self.kvh_toolbar_action.triggered.connect(lambda: self.change_toolbar_visibility(self.kvh_toolbar))

            self.reset_sensor_action.triggered.connect(self.show_reset_sensor_action)
            self.set_imu_heading_action.triggered.connect(self.show_set_imu_heading_action)
            self.set_heading_deg_action.triggered.connect(self.show_set_heading_deg_action)

            self.view_menu_toolbar.addAction(self.kvh_toolbar_action)
            parent.addToolBar(self.kvh_toolbar)

    def show_reset_sensor_action(self):
        self.kvh_geofog3d_ins.reset_sensor()

    def show_set_imu_heading_action(self):
        self.kvh_geofog3d_ins.set_imu_heading()

    def show_set_heading_deg_action(self):
        self.kvh_geofog3d_ins.set_heading_deg()

    def change_toolbar_visibility(self, toolbar):
        """
        Change toolbar visibility

        :param toolbar: toolbar
        """
        sender = self.sender()
        if sender.isChecked():
            toolbar.setVisible(True)
        else:
            toolbar.setVisible(False)

    @staticmethod
    def version():
        """ return version of plugin"""
        return "{} {}".format(iquaview_kvh_geofog3d_ins.PLUGIN_NAME, iquaview_kvh_geofog3d_ins.__version__)


    def remove(self):
        """ Removes module from iquaview"""
        self.kvh_menu.clear()
        self.menubar.removeAction(self.kvh_menu.menuAction())
        self.parent().removeToolBar(self.kvh_toolbar)
        self.view_menu_toolbar.removeAction(self.kvh_toolbar_action)
