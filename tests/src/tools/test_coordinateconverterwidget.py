"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt
from iquaview_lib import resources_rc
from iquaview.src.tools import coordinateconverterwidget


class TestCoordinateConverter(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.coordinateconverter = coordinateconverterwidget.CoordinateConverterDialog()

    def test_converter(self):

        lat = 41.77785501
        lon = 3.03357015

        self.coordinateconverter.coordinate_converter_lineedit.setText("{}, {}".format(lat, lon))

        self.assertAlmostEqual(lat, self.coordinateconverter.coordinate_converter_lineedit.get_latitude(), 7)
        self.assertAlmostEqual(lon, self.coordinateconverter.coordinate_converter_lineedit.get_longitude(), 7)

        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_decimal_degrees(),
                         "41.777855010º N, 003.033570150º E")
        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_degrees_minutes(),
                         "41º 46.6713006' N, 003º 02.0142090' E")
        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_degrees_minutes_seconds(),
                         "41º 46' 40.27804\" N, 003º 02' 00.85254\" E")

if __name__ == '__main__':
    unittest.main()
