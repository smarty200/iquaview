"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtGui import QKeyEvent
from PyQt5.QtCore import QPoint, QEvent, Qt
from PyQt5.QtWidgets import QApplication
from qgis.core import QgsPointXY, QgsDistanceArea
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent

from iquaview_lib.utils.calcutils import distance_ellipsoid
from iquaview.src.mission.maptools.polygontools import (PolygonBy3PointsTool,
                                                        PolygonFromCenterTool,
                                                        PolygonFromCenterFixedTool,
                                                        PolygonByFixedExtentTool)

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestPolygonTools(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.canvas = QgsMapCanvas()

        self.polygon_by_3_points_tool = PolygonBy3PointsTool(self.canvas, 50)
        self.polygon_from_center_tool = PolygonFromCenterTool(self.canvas, 50)

        self.x_length = 45
        self.y_length = 30
        self.polygon_from_center_fixed_tool = PolygonFromCenterFixedTool(self.canvas, self.x_length, self.y_length, 50)
        self.polygon_by_fixed_extent_tool = PolygonByFixedExtentTool(self.canvas, self.x_length, self.y_length, 50)

        self.geom = None

    def test_polygon_by_3_points_tool(self):
        tool = self.polygon_by_3_points_tool
        tool.activate()

        point_1 = QPoint(0, 0)
        point_2 = QPoint(10, 0)
        point_3 = QPoint(10, 10)

        d1 = QgsDistanceArea().measureLine(QgsPointXY(point_1), QgsPointXY(point_2))
        d2 = QgsDistanceArea().measureLine(QgsPointXY(point_2), QgsPointXY(point_3))
        area = d1 * d2

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)
        mouse_move_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_3)
        mouse_press_event_3 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_3, Qt.LeftButton)

        tool.geometry_finished_signal.connect(self.catch_geom_signal)

        # Create the rect geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event_1)
        tool.canvasPressEvent(mouse_press_event_2)
        tool.canvasMoveEvent(mouse_move_event_2)
        tool.canvasPressEvent(mouse_press_event_3)
        # here the tool emits a signal with the geometry created, caught with the connect above

        # area  wont be exact due to the CRS approximations to an spheroid
        self.assertAlmostEqual(self.geom.boundingBox().area(), area, 0)

        # Change segments
        tool.set_segments(3)
        tool.set_segments(4)
        tool.hide_bands()

        # Recreate the geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event_1)
        tool.canvasPressEvent(mouse_press_event_2)
        tool.canvasMoveEvent(mouse_move_event_2)
        tool.canvasPressEvent(mouse_press_event_3)

        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape,  Qt.NoModifier)
        tool.keyReleaseEvent(event_key_escape)

        tool.deactivate()

    def test_polygon_fixed_extent_tool(self):
        tool = self.polygon_by_fixed_extent_tool
        tool.activate()

        point_1 = QPoint(0, 0)
        point_2 = QPoint(10, 0)
        point_3 = QPoint(10, 10)

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)
        mouse_move_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_3)
        mouse_press_event_3 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_3, Qt.LeftButton)

        tool.geometry_finished_signal.connect(self.catch_geom_signal)

        # Create the rect geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event_1)
        tool.canvasPressEvent(mouse_press_event_2)
        tool.canvasMoveEvent(mouse_move_event_2)
        tool.canvasPressEvent(mouse_press_event_3)
        # here the tool emits a signal with the geometry created, caught with the connect above

        box = self.geom.boundingBox()

        # check that the bounding box has the correct size
        l1 = distance_ellipsoid(0, 0, box.width(), 0)
        l2 = distance_ellipsoid(0, 0, 0, box.height())

        self.assertAlmostEqual(l1, self.x_length, 0)
        self.assertAlmostEqual(l2, self.y_length, 0)

        # Update lengths
        self.x_length = self.x_length + 15
        self.y_length = self.y_length + 25
        tool.set_x_length(self.x_length)
        tool.set_y_length(self.y_length)

        box = self.geom.boundingBox()

        # check that the bounding box has the correct size
        l1 = distance_ellipsoid(0, 0, box.width(), 0)
        l2 = distance_ellipsoid(0, 0, 0, box.height())

        self.assertAlmostEqual(l1, self.x_length, 0)
        self.assertAlmostEqual(l2, self.y_length, 0)

        # Change segments
        tool.set_segments(3)
        tool.set_segments(4)
        tool.hide_bands()

        # Recreate the geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event_1)
        tool.canvasPressEvent(mouse_press_event_2)
        tool.canvasMoveEvent(mouse_move_event_2)
        tool.canvasPressEvent(mouse_press_event_3)

        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape,  Qt.NoModifier)
        tool.keyReleaseEvent(event_key_escape)

        tool.deactivate()

    def test_polyogn_from_center_tool(self):
        tool = self.polygon_from_center_tool
        tool.activate()

        point_1 = QPoint(2, 2)
        point_2 = QPoint(3, 3)

        area = abs(point_1.x() - point_2.x())*2 * abs(point_1.y() - point_2.y())*2

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)

        tool.geometry_finished_signal.connect(self.catch_geom_signal)

        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event)
        tool.canvasPressEvent(mouse_press_event_2)

        self.assertAlmostEqual(self.geom.boundingBox().area(), area, 1)

        # Change segments
        tool.set_segments(3)
        tool.set_segments(4)
        tool.hide_bands()

        # Recreate the geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event)
        tool.canvasPressEvent(mouse_press_event_2)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape,  Qt.NoModifier)
        tool.keyPressEvent(event_key_shift)
        tool.keyReleaseEvent(event_key_escape)

        tool.deactivate()

    def test_rect_from_center_fixed_tool(self):
        tool = self.polygon_from_center_fixed_tool
        tool.activate()

        point_1 = QPoint(2, 2)
        point_2 = QPoint(3, 3)

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)

        tool.geometry_finished_signal.connect(self.catch_geom_signal)

        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event)
        tool.canvasPressEvent(mouse_press_event_2)

        box = self.geom.boundingBox()

        # check that the bounding box has the correct size
        l1 = distance_ellipsoid(0, 0, box.width(), 0)
        l2 = distance_ellipsoid(0, 0, 0, box.height())

        self.assertAlmostEqual(l1, self.y_length, 0)
        self.assertAlmostEqual(l2, self.x_length, 0)

        # Update lengths
        self.x_length = self.x_length + 15
        self.y_length = self.y_length + 25
        tool.set_x_length(self.x_length)
        tool.set_y_length(self.y_length)

        box = self.geom.boundingBox()

        # check that the bounding box has the correct size
        l1 = distance_ellipsoid(0, 0, box.width(), 0)
        l2 = distance_ellipsoid(0, 0, 0, box.height())

        self.assertAlmostEqual(l1, self.y_length, 0)
        self.assertAlmostEqual(l2, self.x_length, 0)

        # Change segments
        tool.set_segments(3)
        tool.set_segments(4)
        tool.hide_bands()

        # Recreate the geometry
        tool.canvasPressEvent(mouse_press_event_1)
        tool.canvasMoveEvent(mouse_move_event)
        tool.canvasPressEvent(mouse_press_event_2)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier)
        tool.keyPressEvent(event_key_shift)
        tool.keyReleaseEvent(event_key_escape)

        tool.deactivate()

    def catch_geom_signal(self, geom):
        self.geom = geom


if __name__ == "__main__":
    unittest.main()