"""
Copyright (c) 2020 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest
import time
from unittest.mock import Mock
from copy import copy

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication, QMessageBox

from qgis.gui import QgsMapCanvas
from PyQt5.QtGui import QColor

from iquaview_lib import resources_rc
from iquaview_lib.config import Config

from iquaview.src import resources_qgis
from iquaview.src.canvastracks.gpswidget import GPSWidget

class TestGPSWidget(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = copy(self.config.settings)

        self.canvas = QgsMapCanvas()
        self.canvas.setObjectName("canvas")

        self.gps_widget = GPSWidget(self.canvas, self.config)

    def test_connect(self):
        QMessageBox.critical = Mock()

        self.assertEqual(False, self.gps_widget.is_connected())
        self.gps_widget.connect()
        self.gps_widget.disconnect()
        self.assertEqual(False, self.gps_widget.is_connected())

    def test_gps_update_canvas(self):
        QMessageBox.critical = Mock()

        self.gps_widget.connect()

        # mock gps data
        data = get_mock_data()
        self.gps_widget.gps.get_data = Mock(return_value=data)

        self.gps_widget.gps_update_canvas()

    def test_update_width_and_length(self):
        vessel_name = self.config.csettings['vessel_name']

        self.gps_widget.update_width_and_length()
        self.assertEqual(self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"], self.gps_widget.marker.get_width())
        self.assertEqual(self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"], self.gps_widget.marker.get_length())

    def test_types(self):
        self.assertIsInstance(self.gps_widget.get_status_label_text(), str)
        self.assertIsInstance(self.gps_widget.get_default_color(), QColor)

def get_mock_data():

    data = dict()
    data['time'] = time.time()
    data['latitude'] = 41.77770082113855
    data['longitude'] = 3.0332993934595223
    data['heading'] = 15
    data['quality'] = 1
    data['altitude'] = 0
    data['orientation'] = 15
    data['orientation_time'] = time.time()
    data['status'] = "new_data"

    return data


if __name__ == "__main__":
    unittest.main()