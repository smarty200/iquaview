"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QValidator

from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject, QgsPointXY, QgsWkbTypes

from iquaview.src import resources_qgis
from iquaview_lib import resources_rc
from iquaview_lib.utils.textvalidator import get_color
from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg



class TestPointFeatureDlg(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.canvas = QgsMapCanvas()
        self.proj = QgsProject.instance()

        self.point_feature_dlg = PointFeatureDlg(self.canvas, self.proj)
        self.point_feature_dlg.landmark_added.connect(self.add_map_layer)
        self.point_feature_dlg.reset()

    def test_add_point(self):
        lat = 41.1566
        lon = 3.24556
        self.point_feature_dlg.coordinate_converter_lineedit.setText("{}, {}".format(lat, lon))
        self.point_feature_dlg.accept()

        layers = list(self.proj.mapLayers().values())
        self.assertEqual(1, len(layers))
        self.assertEqual(QgsWkbTypes.PointGeometry, layers[0].geometryType())

        for feature in layers[0].getFeatures():
            wp = feature.geometry().asPoint()
            self.assertAlmostEqual(lat, wp.y(), 8)
            self.assertAlmostEqual(lon, wp.x(), 8)

    def add_map_layer(self, layer):
        self.proj.addMapLayer(layer, True)


if __name__ == "__main__":
    unittest.main()